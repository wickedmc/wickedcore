package de.wicked.core.listeners;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityInteractEvent;

import de.wicked.core.utils.Config;

@SuppressWarnings("unused")
public class EntityListener implements Listener{

	@EventHandler
	public void onDamage(EntityDamageByBlockEvent event) {
		Entity entity = event.getEntity();
		if(entity.hasMetadata("morph")) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		Entity damager = event.getDamager();
		if(entity.hasMetadata("morph")) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		Entity entity = event.getEntity();
		if(entity.hasMetadata("morph")) {
			event.setCancelled(true);
		}
		if(entity instanceof ArmorStand) {
			if(entity.getLocation().getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onInteract(EntityInteractEvent event) {
		Entity entity = event.getEntity();
		if(entity.hasMetadata("morph")) {
			event.setCancelled(true);
		}
		if(entity.getLocation().getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
			event.setCancelled(true);
		}
		
	}
}
