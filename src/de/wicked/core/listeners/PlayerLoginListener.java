package de.wicked.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import de.wicked.core.WickedCore;
import de.wicked.core.bans.BanManager;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class PlayerLoginListener implements Listener{

	BanManager bmanager = new BanManager();
	
	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		
		if(bmanager.isBanned(player.getUniqueId().toString())) {
			if((bmanager.getEnd(player.getUniqueId().toString()) > System.currentTimeMillis())
			|| (bmanager.getEnd(player.getUniqueId().toString()) == -1)) {
					event.disallow(Result.KICK_BANNED, Utils.color("&c&lDu bist gebannt!\n\n&9&nGrund:\n&b"
									+ bmanager.getReason(player.getUniqueId().toString())
									+ "\n\n&9&nVerbleibende Zeit:\n&b"
									+ bmanager.getRemainingTime(player.getUniqueId().toString())));

					WickedCore.debug("[BAN] <"+player.getName()+"> disconnected: Gebannt ("+bmanager.getReason(player.getUniqueId().toString())+")", true);
					if(Config.BROADCAST_FAILJOIN.getBoolean()) {
						Bukkit.broadcastMessage(Utils.color(Messages.FAILJOIN.getString(event.getPlayer().getName(), "Gebannt")));
						return;
					} else {
						return;
					}
			}
		}
		
		if(!event.getAddress().getHostAddress().equals(Config.PROXYIP.getString())) {
			event.disallow(Result.KICK_OTHER, "&cBitte joine mit der IP WickedMc.de!");
		}
	}
	
}
