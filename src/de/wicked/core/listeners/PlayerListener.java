package de.wicked.core.listeners;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.PlayerMode;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.menus.NavigationMenu;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onInteractAtEntity(PlayerInteractAtEntityEvent event) {
		if(WickedCore.getUser(event.getPlayer()).getPlayerMode() == PlayerMode.LOBBY
				|| event.getRightClicked().hasMetadata("morph")) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		WickedUser user = WickedCore.getUser(player);
		ItemStack hand = player.getItemInHand();
		
		if(user.getPlayerMode() == PlayerMode.LOBBY) {
			event.setCancelled(true);
			if(hand.getType() == Material.COMPASS) {
				WickedCore.getMenuManager().getMenu("nav").show(player);
			} else if(hand.getType() == Material.CHEST) {
				WickedCore.getMenuManager().getMenu("cosmetic").show(player);
				player.playSound(player.getLocation(), Sound.CHEST_OPEN, 0.3f, 0.8f);
			}
		}
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent event) {
		if(event.getWhoClicked() instanceof Player) {
			if(WickedCore.getUser((Player)event.getWhoClicked()).getPlayerMode() == PlayerMode.LOBBY) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		if(WickedCore.getUser(event.getPlayer()).getPlayerMode() == PlayerMode.LOBBY) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if(WickedCore.getUser(event.getPlayer()).getPlayerMode() == PlayerMode.LOBBY) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPvP(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			if(WickedCore.getUser((Player)event.getEntity()).getPlayerMode() == PlayerMode.LOBBY
				|| WickedCore.getUser((Player)event.getDamager()).getPlayerMode() == PlayerMode.LOBBY) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			if(WickedCore.getUser((Player)event.getEntity()).getPlayerMode() == PlayerMode.LOBBY) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onStarve(FoodLevelChangeEvent event) {
		if(event.getEntity() instanceof Player) {
			if(WickedCore.getUser((Player)event.getEntity()).getPlayerMode() == PlayerMode.LOBBY) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		if(WickedCore.getUser(event.getPlayer()).getPlayerMode() == PlayerMode.LOBBY) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent event) {
		if(WickedCore.getUser(event.getPlayer()).getPlayerMode() == PlayerMode.LOBBY) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event) {
		Player player = event.getPlayer();
		WickedUser user = WickedCore.getUser(player);
		
		if(!(user.isLoaded())) {
			return;
		}
		
		if(user.getActiveGadget().isRunning(player)) {
			user.getActiveGadget().cancel(player);
		}
		
		if(user.getMorph() != null && user.getMorph().isRunning()) {
			user.getMorph().cancel();
		}
		
		if(event.getFrom().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) { 
			user.setPlayerMode(PlayerMode.OTHER);
		}
		
		if(player.getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
			user.setPlayerMode(PlayerMode.LOBBY);
		}
		
		if(player.getWorld().getName().equalsIgnoreCase(Config.FREEBUILDWORLD.getString())) {
			player.setGameMode(GameMode.SURVIVAL);
			
			// Inventar laden
			
		}
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if(player.getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
			try {
				if(player.getLocation().getY() < 10) {
					player.teleport(NavigationMenu.getLoc("spawn"));
				}
			} catch(NullPointerException ex) { }
		}
	}
}
