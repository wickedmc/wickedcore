package de.wicked.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.Config;

public class ServerListener implements Listener{

	@EventHandler
	public void onWeather(WeatherChangeEvent event) {
		if(event.getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
			try {
			Bukkit.getWorld(Config.LOBBYWORLD.getString()).setStorm(false);
			} catch(Exception ex) {
				WickedCore.debug("Immernoch Fehler beim WeatherChange bzw setStorm(false)", true);
			}
			event.setCancelled(true);
		}
	}
	
}
