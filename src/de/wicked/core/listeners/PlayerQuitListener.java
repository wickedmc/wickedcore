package de.wicked.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.ProfileSaver;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class PlayerQuitListener implements Listener{

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		event.setQuitMessage(null);
		
		// User speichern
		WickedUser user = WickedCore.getUser(player);
		
		if(player.getWorld().getName().equalsIgnoreCase(Config.FREEBUILDWORLD.getString())) {
					user.setLastLoc(player.getLocation());
					user.setLastInv(player.getInventory());
		}
		
		try {
			String msg = Messages.QUITMSG.getString();
			msg = msg.replaceAll("%r", user.getRank().prefix());
			msg = msg.replaceAll("%p", user.getActivePrefix());
			msg = msg.replaceAll("%c", user.getRank().color());
			msg = msg.replaceAll("%n", user.getName());
			msg = msg.replaceAll("%d", user.getDisplayName());
			Bukkit.broadcastMessage(Utils.color(msg));
		} catch(NullPointerException ex) {
			WickedCore.debug("NullPointerException (PlayerQuitListener:onQuit()) (Player="+player.getName()+")", true);
			ex.printStackTrace();
		}
		
		try {
			WickedCore.getTask(player).cancel();
			WickedCore.removeTask(player);
		} catch(Exception ex) {}
		
		user.setOnTime();
		user.setLoaded(false);
		WickedCore.getRemovedUser(player);
		new ProfileSaver(user).runTaskAsynchronously(WickedCore.getInstance());
		
	}
	
}
