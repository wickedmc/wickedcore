package de.wicked.core.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.bans.BanManager;
import de.wicked.core.profiles.OnTimeScheduler;
import de.wicked.core.profiles.PlayerMode;
import de.wicked.core.profiles.ProfileLoader;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;
import de.wicked.core.utils.menus.NavigationMenu;

public class PlayerJoinListener implements Listener{

	BanManager bmanager = new BanManager();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		event.setJoinMessage(null);
		// Entbannen
		if(bmanager.isBanned(player.getUniqueId().toString())) {
			bmanager.unban(player.getUniqueId().toString());
			WickedCore.debug("[BAN] <"+player.getName()+"> entbannt", true);
		}
		// User laden
		WickedUser user = new WickedUser(player);
		new ProfileLoader(user).runTaskAsynchronously(WickedCore.getInstance());
		WickedCore.addUser(user);
			new BukkitRunnable() {
				@Override
				public void run() {
					try {
						
						if(!user.isLoaded()) {
							player.kickPlayer("�cDatenbank Fehler. Bitte versuch es sp�ter erneut.");
							WickedCore.debug("Spieler "+player.getName()+" gekickt: Datenbankfehler (PlayerJoinListener)", true);
							return;
						}
						
						String msg = Messages.JOINMSG.getString();
						msg = msg.replaceAll("%r", user.getRank().prefix());
						msg = msg.replaceAll("%p", user.getActivePrefix());
						msg = msg.replaceAll("%c", user.getRank().color());
						msg = msg.replaceAll("%n", user.getName());
						msg = msg.replaceAll("%d", user.getDisplayName());
						Bukkit.broadcastMessage(Utils.color(msg));
						
						if(user.hasPermission(Rank.ADMIN)) {
							if(user.getBirthday() == null) {
								player.sendMessage(Utils.color("&6[Geburtstag] &7Trag deinen Geburtstag ein und bekomme tolle Geschenke! &e/geburtstag"));
							} else {
								// TODO Gebutstags check und so
								WickedCore.debug(player.getName()+"'s Bday: "+user.getBirthday().toString(), false);
							}
						}
						
						if(user.getPlayerMode() == PlayerMode.LOBBY) {
							player.teleport(NavigationMenu.getLoc("spawn"));
							
							if(player.getLastPlayed() == 0) {
								Utils.setLobby(player);
								player.sendMessage(Utils.color(""));
								player.sendMessage(Utils.color("&2&lWillkommen auf &a&lWickedMc.de&2&l!"));
								player.sendMessage(Utils.color("&8> &7Nutze den Kompass, um zu einem Portal zu tp'n"));
								player.sendMessage(Utils.color("&8> &7Mit &6/lobby&7 kommst du wieder zur�ck!"));
								player.sendMessage(Utils.color("&2&lViel Spa�!"));
								player.sendMessage(Utils.color(""));
							}
						}
//						
					} catch(NullPointerException ex) {
						WickedCore.debug("NullPointerException (PlayerJoinListener:onJoin()) (Player="+player.getName()+")", true);
						ex.printStackTrace();
					}
					if(Config.ONTIMEMONEY.getBoolean()) {
						BukkitRunnable task = new OnTimeScheduler(player, Config.ONTIMEMONEY_TIME.getInt(), Config.ONTIMEMONEY_MONEY.getDouble());
						task.runTaskTimerAsynchronously(WickedCore.getInstance(), 20*60*Config.ONTIMEMONEY_TIME.getInt(), 20*60*Config.ONTIMEMONEY_TIME.getInt());
						WickedCore.addTask(player, task);
					}
				}
			}.runTaskLater(WickedCore.getInstance(), 10);
	} 
}
