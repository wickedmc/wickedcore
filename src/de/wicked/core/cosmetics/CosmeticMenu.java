package de.wicked.core.cosmetics;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;
import de.wicked.core.utils.menus.Menu;

public class CosmeticMenu extends Menu{

	@SuppressWarnings("unused")
	public CosmeticMenu(String title) {
		super(title, 36);
		
		ItemStack gadgets = new ItemStackBuilder(Material.PISTON_BASE)
								.withName("&aGadgets").withLore("")
								.withLore("&7Vertreib dir mit coolen Gadgets die Zeit!").build();
		ItemStack morphs = new ItemStackBuilder(Material.MONSTER_EGG)
								.withName("&2Morphs").withLore("")
								.withLore("&7Verwandle dich in einen Mob!")
								.withDurability(50).build();
		ItemStack hats = new ItemStackBuilder(Material.DIAMOND_HELMET)
								.withName("&bH�te").withLore("")
								.withLore("&7Stylische H�te!!")
								.withLore("").withLore("&4&lCOMING SOON!").build();
		ItemStack pets = new ItemStackBuilder(Material.LEASH)
								.withName("&6Haustiere").withLore("")
								.withLore("&7S��e Haustiere zum Liebhaben!")
								.withLore("").withLore("&4&lCOMING SOON!").build();
		ItemStack cancel = new ItemStackBuilder(Material.REDSTONE_BLOCK)
								.withName("&cAbbrechen").build();
		ItemStack info = new ItemStackBuilder(Material.GLOWSTONE_DUST)
								.withName("&ePremium").withLore("")
								.withLore("&7Jetzt kostenlos testen!").build();
		
		getInventory().setItem(10, gadgets);
		getInventory().setItem(12, morphs);
		getInventory().setItem(14, hats);
		getInventory().setItem(16, pets);
		//getInventory().setItem(30, info);
		getInventory().setItem(31, cancel);
		
	}

	@Override
	public void click(Player player, ItemStack itemStack) {
		try {
			
			String itemName = getFriendlyName(itemStack);
			
			switch(itemName) {
			
			case "Gadgets":
				WickedCore.getMenuManager().getMenu("gadgets").show(player); return;
			
			case "Morphs":
				WickedCore.getMenuManager().getMenu("morphs").show(player); return;
				
			case "H�te":
				WickedCore.getMenuManager().getMenu("hats").show(player); return;
				
			case "Haustiere":
				WickedCore.getMenuManager().getMenu("pets").show(player); return;
				
			case "Abbrechen":
				player.closeInventory(); return;
			
			case "Premium":
				
				/*
				WickedUser user = WickedCore.getUser(player);
				if(user.hasPremiumTested()) {
					player.sendMessage(Utils.color("&cDu hast Premium bereits getestet!"));
					player.sendMessage(Utils.color("&cDu kannst dir Premium kaufen! /premium"));
				} else {
					user.setPremiumTested(true);
					user.setPremiumTestStart(System.currentTimeMillis());
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "setpremium "+player.getName());
					
					WickedCore.getMySQL().update("INSERT INTO TestPremium(UUID,fertig,testBeginn,testEnde) "
							+ "VALUES('"+player.getUniqueId()+"','0','"+System.currentTimeMillis()+"','"
							+System.currentTimeMillis()+Utils.stringToMillies("7d"));
					
					player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
					player.sendMessage(Utils.color("&8-[]=-- &6&l-=[> <]=- &8--=[]-"));
					player.sendMessage(Utils.color("&8\u27A4 &eDeine 7 Tage Premium Testphase hat begonnen!"));
					player.sendMessage(Utils.color("&8\u27A4 &eSie l�uft am &6"+Utils.milliesToDate(Utils.stringToMillies("7d")) + "&e aus."));
					player.sendMessage(Utils.color("&8\u27A4 &eWir w�nschen Dir viel Spa�!"));
					player.sendMessage(Utils.color("&8-[]=-- &6&l-=[> <]=- &8--=[]-"));
				}
				*/
			
			}
			
			
		} catch(Exception ex) { }
	}

}
