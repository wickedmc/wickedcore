package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.slikey.effectlib.util.ParticleEffect;
import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;

public class RainbowGadget extends Gadget{

	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>(); // Tasks der Spieler zum sp�teren canceln.
	
	public RainbowGadget(int cooldown, String name) {
		super(cooldown, name, new ItemStackBuilder(Material.LEATHER_HELMET)
		.withColor(Color.YELLOW)
		.withName("&4Reg&cen&ebo&agen &bR�&9st&5ung")
		.withLore("")
		.withLore("&7&oViele, viele bunte... R�stungen")
		.withLore("")
		.withLore("&8Cooldown: &e40s")
		.withLore("&8Dauer: &e20s")
		.build());
	}

	@Override
	public void use(Player p) {
		if(!checkCooldown(p)) {
			return;
		}
		
		tasks.put(p, new BukkitRunnable() {
			
			ItemStackBuilder helm = new ItemStackBuilder(Material.LEATHER_HELMET);
			ItemStackBuilder chest = new ItemStackBuilder(Material.LEATHER_CHESTPLATE);
			ItemStackBuilder hose = new ItemStackBuilder(Material.LEATHER_LEGGINGS);
			ItemStackBuilder boots = new ItemStackBuilder(Material.LEATHER_BOOTS);
			
			Color[] farben = {Color.AQUA,Color.BLUE,Color.FUCHSIA,Color.GREEN,Color.LIME,Color.NAVY,Color.ORANGE,
							  Color.RED, Color.MAROON, Color.YELLOW}; // 10
			
			int h=0,c=1,l=3,b=4; // h=helm c=chest l=leggings b=boots, verschieden starten und durchgehn
			private int countdown = 4*20;
			
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					RainbowGadget.this.cancel(p);
					return;
				}
				
				Random r = new Random();
				
				p.getInventory().setHelmet(helm.withColor(farben[r.nextInt(10)]).build());
				p.getInventory().setChestplate(chest.withColor(farben[r.nextInt(10)]).build());
				p.getInventory().setLeggings(hose.withColor(farben[r.nextInt(10)]).build());
				p.getInventory().setBoots(boots.withColor(farben[r.nextInt(10)]).build());
				
				if(countdown % 3 == 0) {
					p.getWorld().playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					ParticleEffect effect = ParticleEffect.REDSTONE;
					effect.display(1, 1, 1, 1, 30, p.getLocation(), 50);
				}
				
				h=plus(h); c=plus(c); l=plus(l); b=plus(b);
				
				countdown--;
			}
		});
		
		// Task von unten ausf�hren
		tasks.get(p).runTaskTimer(WickedCore.getInstance(), 0, 5);
	}

	private int plus(int i) {
		i++;
		if(i > 9) {
			i=0;
		}
		return i;
	}
	
	public void cancel(Player p) {
		p.getInventory().setArmorContents(null);
		tasks.get(p).cancel();
		
		tasks.remove(p);
	}
	
	public boolean isRunning(Player p) {
		return tasks.get(p) == null ? false:true;
	}
}
