package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

import de.slikey.effectlib.util.ParticleEffect;
import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;

public class HellfireGadget extends Gadget implements Listener{

	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>(); // Tasks der Spieler zum sp�teren canceln.
	
	public HellfireGadget(int cooldown, String name) {
		super(cooldown, name, new ItemStackBuilder(Material.BOW)
		.withName("&cHellfire")
		.withLore("")
		.withLore("&7&oPew pew pew! BOM BOM BOM!")
		.withLore("")
		.withLore("&8Cooldown: &e20s")
		.withLore("&8Dauer: &e10s")
		.build());
		Bukkit.getPluginManager().registerEvents(this, WickedCore.getInstance());
	}

	@Override
	public void use(Player p) {
		if(!checkCooldown(p)) {
			return;
		}
		
		tasks.put(p,
		new BukkitRunnable() {
			
			int countdown = 20;
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					HellfireGadget.this.cancel(p);
					return;
				}
				
				Entity e = p.shootArrow();
				e.setMetadata("hellfire", new FixedMetadataValue(WickedCore.getInstance(), true));
				
				countdown--;
			}
		});
		
		// HIER IST BEGINN
		
		// Task ausf�hren.
		tasks.get(p).runTaskTimer(WickedCore.getInstance(), 0, 10);
		
	}
	
	public void cancel(Player p) {
		tasks.get(p).cancel();

		tasks.remove(p);
	}
	
	public boolean isRunning(Player p) {
		return tasks.get(p) == null ? false:true;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onHit(ProjectileHitEvent event) {
		Projectile proj = event.getEntity();
		if(proj instanceof Arrow) {
			if(proj.hasMetadata("hellfire")) {
				ParticleEffect effect = ParticleEffect.EXPLOSION_LARGE;
				effect.display(proj.getLocation(), 20, 0, 0, 0, 0, 1);
				ParticleEffect.FLAME.display(proj.getLocation(), 20);
				proj.getWorld().playSound(proj.getLocation(), Sound.EXPLODE, 1, 1);
				proj.remove();
			}
		}
	}
	
}
