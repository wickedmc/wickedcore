package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.slikey.effectlib.util.ParticleEffect;
import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;

public class RocketGadget extends Gadget{

	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>();
	
	public RocketGadget(int cooldown, String name) {
		super(cooldown, name, new ItemStackBuilder(Material.FEATHER)
		.withName("&9Superjump")
		.withLore("")
		.withLore("&7&oBis zur Unendlichkeit...!")
		.withLore("")
		.withLore("&8Cooldown: &e15s")
		.withLore("&8Dauer: &e4s")
		.build());
	}


	@Override
	public void use(Player p) {

		if(!checkCooldown(p)) { return; }
		
		p.setVelocity(p.getLocation().getDirection().setY(10));
		p.getWorld().playSound(p.getLocation(), Sound.EXPLODE, 1, 1);
		
		tasks.put(p, new BukkitRunnable() {
			
			int countdown = 20*2;
			
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					RocketGadget.this.cancel(p);
					return;
				}

				p.getWorld().playEffect(p.getLocation(), Effect.SMOKE, 1);
				p.getWorld().playEffect(p.getLocation(), Effect.FIREWORKS_SPARK, 1);
				p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_LAUNCH, 1, 1);
				
				countdown--;
			}
		});
		
		
		tasks.get(p).runTaskTimer(WickedCore.getInstance(), 0, 1);
		
	}

	@Override
	public boolean isRunning(Player p) {
		return tasks.containsKey(p);
	}


	@SuppressWarnings("deprecation")
	@Override
	public void cancel(Player p) {
		ParticleEffect expl = ParticleEffect.EXPLOSION_HUGE;
		ParticleEffect firew = ParticleEffect.FIREWORKS_SPARK;
		
		expl.display(p.getLocation(), 50, 0, 0, 0, 0, 1);
		firew.display(1, 1, 1, 1, 30, p.getLocation(), 3);
		ParticleEffect.REDSTONE.display(1, 1, 1, 1, 50, p.getLocation(), 50);
		ParticleEffect.FLAME.display(1, 1, 1, 1, 50, p.getLocation(), 50);
		
		p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_LARGE_BLAST, 1, 1);
		p.getWorld().playSound(p.getLocation(), Sound.FIREWORK_TWINKLE, 1, 1);
		
		tasks.get(p).cancel();
		tasks.remove(p);
	}
	
}
