package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;
import de.wicked.core.utils.Utils;

public class FireworksGadget extends Gadget{
	
	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>();

	public FireworksGadget(int cooldown, String name) {
		super(cooldown, name,  new ItemStackBuilder(Material.FIREWORK)
		.withName("&cFeuerwerk")
		.withLore("")
		.withLore("&7&oWerde zu Sylvester!")
		.withLore("&7&o...nicht Stalone, nur Sylvester...")
		.withLore("")
		.withLore("&8Cooldown: &e60s")
		.withLore("&8Dauer: &e10s")
		.build());
	}

	@Override
	public void use(Player p) {

		if(!checkCooldown(p)) { return; }
		
		tasks.put(p, new BukkitRunnable() {
			
			int countdown = 10;
			
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					FireworksGadget.this.cancel(p);
					return;
				}
				
				Location location = p.getLocation();
				
				for(int i = 0 ; i < 5 ; i++) {
			        //Random Feuerwerk an random Location spawnen
		            Firework firework = (Firework) location.getWorld().
		            					spawnEntity(Utils.randomRadiusLoc(location, 5), EntityType.FIREWORK);
		            firework.setFireworkMeta(Utils.randomFireworkMeta(firework));
		        }
				

				
				countdown--;
			}
		});

		tasks.get(p).runTaskTimer(WickedCore.getInstance(), 0, 20);
	}

	@Override
	public void cancel(Player p) {
		
		
		tasks.get(p).cancel();
		tasks.remove(p);
	}

	@Override
	public boolean isRunning(Player p) {
		return tasks.containsKey(p);
	}


	
}
