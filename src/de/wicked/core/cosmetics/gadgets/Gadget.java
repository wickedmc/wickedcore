package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Utils;

public abstract class Gadget {

	private int cooldown;
	private String name;
	private ItemStack item;
	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>();
	
	public Gadget(int cooldown, String name, ItemStack item) {
		this.cooldown = cooldown*1000;
		this.name = name;
		this.item = item;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public int getCooldown() {
		return cooldown;
	}
	
	public String getName() {
		return name;
	}
	
	public HashMap<Player, BukkitRunnable> getTasks() {
		return tasks;
	}
	
	public abstract void cancel(Player p);
	
	public abstract void use(Player p);
	
	public abstract boolean isRunning(Player p);
	
	public void start(Player p) {
		if(checkCooldown(p)) {
			use(p);
		}
	}
	
	public boolean checkCooldown(Player p) {
		WickedUser user = WickedCore.getUser(p);
		long difference = System.currentTimeMillis()-user.getLastGadgetUse();
		if(difference <= getCooldown()) {
			user.getPlayer().sendMessage(Utils.color("&cDu musst noch &e" + Utils.milliesToString(getCooldown()-difference) + " &cwarten!"));
			return false;
		}
		
		user.setLastGadgetUse(System.currentTimeMillis());
		return true;
	}


}
