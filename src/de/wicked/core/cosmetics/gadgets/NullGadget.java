package de.wicked.core.cosmetics.gadgets;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class NullGadget extends Gadget{

	public NullGadget() {
		super(0, "", new ItemStack(Material.AIR));
	}

	@Override
	public void use(Player p) {
		return;
	}

	@Override
	public void cancel(Player p) {
	}

	@Override
	public boolean isRunning(Player p) {
		return false;
	}

}
