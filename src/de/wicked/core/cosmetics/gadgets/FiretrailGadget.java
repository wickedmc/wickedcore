package de.wicked.core.cosmetics.gadgets;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.slikey.effectlib.util.ParticleEffect;
import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;

public class FiretrailGadget extends Gadget{
 // Tasks der Spieler zum sp�teren canceln.
	
	public FiretrailGadget(int cooldown, String name) {
		super(cooldown, name, new ItemStackBuilder(Material.BLAZE_POWDER)
		.withName("&6Firetrail")
		.withLore("")
		.withLore("&7&oMach dir Feuer unterm Hintern!")
		.withLore("")
		.withLore("&8Cooldown: &e40s")
		.withLore("&8Dauer: &e7s")
		.build());
	}

	@Override
	public void use(final Player p) {
		if(!checkCooldown(p)) {
			return;
		}
		
		getTasks().put(p,
		new BukkitRunnable() {
			
			int countdown = 20*7;
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					FiretrailGadget.this.cancel(p);
					return;
				}
				
				Location loc = p.getLocation();
				Block block = loc.getBlock();
				
				if(block.getType() == Material.AIR) {
					
					block.setTypeIdAndData(51, (byte) 0, true);
					p.setFireTicks(0);
					ParticleEffect effect = ParticleEffect.FLAME;
					effect.display(1, 1, 1, 0, 5, p.getLocation(), 1);
					
					new BukkitRunnable() {
						@Override
						public void run() {
							block.setType(Material.AIR);
							p.setFireTicks(0);
						}
					}.runTaskTimer(WickedCore.getInstance(), 20*2, 1);
					
					countdown--;
				}
			}
			
		});
		
		// Hier ist Start!
		
		p.setWalkSpeed(0.5f);
		p.playSound(p.getLocation(), Sound.FIRE_IGNITE, 1, 1);

		// Task ausf�hren.
		getTasks().get(p).runTaskTimer(WickedCore.getInstance(), 0, 1);
		
	}
	
	public void cancel(Player p) {
		try {
			p.setWalkSpeed(0.2f);
			p.playSound(p.getLocation(), Sound.FUSE, 1, 1);
			p.setFireTicks(0);
			getTasks().get(p).cancel();
			
			getTasks().remove(p);
		} catch(Exception ex) {
			
		}
	}
	
	public boolean isRunning(Player p) {
		return getTasks().get(p) == null ? false:true;
	}
}
