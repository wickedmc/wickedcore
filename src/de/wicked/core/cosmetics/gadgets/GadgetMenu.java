package de.wicked.core.cosmetics.gadgets;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.ItemStackBuilder;
import de.wicked.core.utils.Utils;
import de.wicked.core.utils.menus.Menu;

public class GadgetMenu extends Menu{

	public GadgetMenu(String title) {
		super(title, 27);
		
		ItemStack abbrechen = new ItemStackBuilder(Material.REDSTONE_BLOCK).withName("&cAbbrechen").build();
		ItemStack reset = new ItemStackBuilder(Material.BARRIER).withName("&cZur�cksetzen").build();
		ItemStack back = new ItemStackBuilder(Material.ARROW).withName("&eZur�ck").build();

		getInventory().setItem(20, back);
		getInventory().setItem(22, abbrechen);
		getInventory().setItem(24, reset);
		
		for(Gadget g : WickedCore.getGadgetManager().getGadgets()) {
			getInventory().addItem(g.getItem());
		}
	}

	@Override
	public void click(Player player, ItemStack itemStack) {
		try {
			
			String itemName = getFriendlyName(itemStack);
			
			switch(itemName) {
			case "Abbrechen":
				player.closeInventory();
				player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 0.1f, 0.1f);
				return;
			case "Zur�cksetzen":
				System.out.println(WickedCore.getUser(player).getActiveGadget().isRunning(player));
				if(WickedCore.getUser(player).getActiveGadget().isRunning(player)) {
					WickedCore.getUser(player).getActiveGadget().cancel(player);
				}
				player.getInventory().setItem(5, new ItemStack(Material.AIR));
				player.closeInventory();
				player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 0.1f, 0.1f);
				WickedCore.getUser(player).setActiveGadget(new NullGadget());
				player.sendMessage(Utils.color("&cGadget zur�ckgesetzt."));
				return;
			case "Zur�ck":
				WickedCore.getMenuManager().getMenu("cosmetic").show(player);
				return;
			default:
				if(!WickedCore.getUser(player).hasPermission(Rank.PREMIUM)) {
					player.sendMessage(Utils.color("&cGadgets sind nur f�r &6Premiumspieler"));
					return;
				}
				if(itemStack.getType() != Material.AIR) {
					
					if(WickedCore.getUser(player).getActiveGadget().isRunning(player)) {
						player.sendMessage(Utils.color("&cDu hast bereits ein aktives Gadget"));
						player.closeInventory();
						return;
					}
					
					player.getInventory().setItem(5, itemStack);
					player.closeInventory();
					WickedCore.getUser(player).setActiveGadget(WickedCore.getGadgetManager().getGadget(itemStack));
					player.sendMessage(Utils.color("&aGadget &e"+itemStack.getItemMeta().getDisplayName()+" &aausgew�hlt."));
				}
				return;
			}
			
			
		} catch(Exception ex) {
			
		}
	}

	
	
}
