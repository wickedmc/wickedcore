package de.wicked.core.cosmetics.gadgets;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.PlayerMode;

public class GadgetManager implements Listener{

	// gadgetideen:
	// superjump, astro creeper, rocket
	
	private Map <Object, Gadget> gadgets = new HashMap<>();
	
	public GadgetManager(Plugin plugin) {
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	public Collection<Gadget> getGadgets() {
		return gadgets.values();
	}
	
	public void addGadget(Object key, Gadget gadget) {
		gadgets.put(key, gadget);
	}
	
	public Gadget getGadget(Object key) {
		return gadgets.get(key);
	}
	
	public Gadget getGadget(String name) {
		for(Gadget g : gadgets.values()) {
			if(g.getName().equalsIgnoreCase(name)) {
				return g;
			}
		}
		return new NullGadget();
	}
	
	public Gadget getGadget(ItemStack item) {
		for(Gadget g : gadgets.values()) {
			if(g.getItem().equals(item)) {
				return g;
			}
		}
		return new NullGadget();
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		ItemStack item = p.getItemInHand();
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK
		|| event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
			
			for(Gadget g : gadgets.values()) {
				if(item.equals(g.getItem())) {
					if(WickedCore.getUser(p).getPlayerMode() == PlayerMode.LOBBY) {
						event.setCancelled(true);
						g.use(p);
					}
				}
			}
			
		}
	}
}
