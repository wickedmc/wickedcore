package de.wicked.core.cosmetics.gadgets;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.ItemStackBuilder;

public class LavatrailGadget extends Gadget{

	private HashMap<Player, BukkitRunnable> tasks = new HashMap<>(); // Tasks der Spieler zum sp�teren canceln.
	
	public LavatrailGadget(int cooldown, String name) {
		super(cooldown, name, new ItemStackBuilder(Material.LAVA_BUCKET)
		.withName("&eLavatrail")
		.withLore("")
		.withLore("&7&oHot!")
		.withLore("")
		.withLore("&8Cooldown: &e40s")
		.withLore("&8Dauer: &e5s")
		.build());
	}

	@Override
	public void use(Player p) {
		
		if(!checkCooldown(p)) {
			return;
		}
		tasks.put(p, new BukkitRunnable() {
			
			int countdown = 20*5;
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				
				if(countdown <= 0 || !p.isOnline()) {
					LavatrailGadget.this.cancel(p);
					return;
				}
				
				Block block = p.getLocation().getBlock();
				
				
				if(block.getType() == Material.AIR) {
					
					new BukkitRunnable() {
						@Override
						public void run() {
							
							if(!block.equals(p.getLocation().getBlock()) && p.isOnGround()) {
								block.setTypeIdAndData(11, (byte) 7, true);
								
								new BukkitRunnable() {
									@Override
									public void run() {
										block.setType(Material.AIR);
									}
								}.runTaskLater(WickedCore.getInstance(), 20*3);
								
								
							}
							
						}
					}.runTaskLater(WickedCore.getInstance(), 5);
				}
				
				countdown--;
				
			}
		});
		
		// HIER IST ANFANG
		p.setWalkSpeed(0.6f);
		
		// Task starten
		tasks.get(p).runTaskTimer(WickedCore.getInstance(), 0, 1);
		
	}

	public void cancel(Player p) {
		p.setWalkSpeed(0.2f);
		tasks.get(p).cancel();
		
		tasks.remove(p);
	}

	public boolean isRunning(Player p) {
		return tasks.get(p) == null ? false:true;
	}
	
}
