package de.wicked.core.cosmetics.morphs;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.ItemStackBuilder;
import de.wicked.core.utils.Utils;
import de.wicked.core.utils.menus.Menu;

public class MorphMenu extends Menu{

	public MorphMenu(String title) {
		super(title, 36);

		ItemStack abbrechen = new ItemStackBuilder(Material.REDSTONE_BLOCK).withName("&cAbbrechen").build();
		ItemStack reset = new ItemStackBuilder(Material.BARRIER).withName("&cZur�cksetzen").build();
		ItemStack back = new ItemStackBuilder(Material.ARROW).withName("&eZur�ck").build();

		getInventory().setItem(29, back);
		getInventory().setItem(31, abbrechen);
		getInventory().setItem(33, reset);
		
		for(MorphType t : MorphType.values()) {
			getInventory().addItem(new ItemStackBuilder(Material.MONSTER_EGG)
					.withName(t.getName()).withDurability(t.data())
					.withLore("") .withLore("&7&oVerwandle dich in ein cooles Tier!") .withLore("")
					.withLore("&8Cooldown: &e20s") .withLore("&8Dauer: &e3min").build());
		}
	}

	@Override
	public void click(Player player, ItemStack itemStack) {
	try {
			String itemName = getFriendlyName(itemStack);
			WickedUser user = WickedCore.getUser(player);
			
			switch(itemName) {

			case "Abbrechen":
				closeChest(player);
				return;

			case "Zur�cksetzen":
				if(user.getMorph() == null) { player.sendMessage(Utils.color("&cDu bist nicht gemorpht!"));
				closeChest(player); return; }
				
				if(user.getMorph().isRunning()) { user.getMorph().cancel(); }

				closeChest(player);
				player.sendMessage(Utils.color("&cMorph abgebrochen."));
				return;
				
			case "Zur�ck":
				WickedCore.getMenuManager().getMenu("cosmetic").show(player);
				return;
			}
			
			if(itemStack.getType() == Material.MONSTER_EGG) {
				
				if(!user.hasPermission(Rank.PREMIUM)) {
					player.sendMessage(Utils.color("&cMorphs sind nur f�r &6Premiumspieler")); return;
				}

				if(user.getMorph() != null) { player.sendMessage(Utils.color("&cDu bist bereits gemorpht!"));
				closeChest(player); return; }
				
				
				int data = itemStack.getDurability();
				MorphType morph = MorphType.getByData(data);
				if(morph == null) { player.sendMessage(Utils.color("&cFehler.")); closeChest(player); }
				if( new Morph(player, morph, 3, 20).start() ) player.sendMessage(Utils.color("&aMorph " + morph.getName() + " &aaktiviert!"));
				return;
			}
			
		} catch(Exception ex) {}
	}

	private void closeChest(Player player) {
		player.closeInventory();
		player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 0.1f, 0.1f);
	}
	
}
