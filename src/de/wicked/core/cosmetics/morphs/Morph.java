package de.wicked.core.cosmetics.morphs;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Utils;

public class Morph implements Listener{
	
	private BukkitRunnable task;
	private Entity morph;
	private MorphType type;
	private Player player;
	private WickedUser user;
	private boolean running;
	private int countdown;
	private int cooldown;
	private int duration;
	
	public Morph(Player player, MorphType type, int duration, int cooldown) {

		Bukkit.getPluginManager().registerEvents(this, WickedCore.getInstance());
		
		this.player = player;
		user = WickedCore.getUser(player);
		this.cooldown = cooldown*1000;
		this.duration = (duration*60)*20;
		this.countdown = this.duration;
		this.type = type;
		
		task = new BukkitRunnable() {
			@Override
			public void run() {
				
				if(countdown <= 0 || !player.isOnline()) {
					player.sendMessage(Utils.color("&cDein Morph ist abgelaufen"));
					Morph.this.cancel();
					return;
				}
				
				morph.teleport(player);
				morph.setFireTicks(0);
				
				countdown--;
			}
		};
	}
	
	public boolean start() {
		
		long diff = System.currentTimeMillis() - user.getLastMorphUse();
		if(diff <= cooldown) {
			user.getPlayer().sendMessage(Utils.color("&cDu musst noch &e" + Utils.milliesToString(cooldown - diff) + " &cwarten!"));
			return false;
		}
		
		morph = player.getWorld().spawnEntity(player.getLocation(), type.getEntity());
		morph.setCustomName(Utils.color(user.getRank().color()+user.getDisplayName()));
		morph.setMetadata("morph", new FixedMetadataValue(WickedCore.getInstance(), true));
		
		task.runTaskTimer(WickedCore.getInstance(), 0, 1);
		
//		for(Player plr : Bukkit.getOnlinePlayers()) { plr.hidePlayer(player); }
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (int)Math.pow(10, 80), 1, false, false));
		user.setMorph(this);
		
		running = true;
		return true;
	}
	
	public void cancel() {
		
//		for(Player plr : Bukkit.getOnlinePlayers()) { plr.showPlayer(player); }
		player.removePotionEffect(PotionEffectType.INVISIBILITY);
		user.setMorph(null);
		user.setLastMorphUse(System.currentTimeMillis());
		
		morph.remove();
		
		task.cancel();
		running = false;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	@EventHandler
	public void onExplode(EntityExplodeEvent event) {
		if(event.getEntity().hasMetadata("morph")) { event.setCancelled(true); }
	}
	
	@EventHandler
	public void onTarget(EntityTargetLivingEntityEvent event) {
		if(event.getEntity().hasMetadata("morph")) { event.setCancelled(true); }
	}
	
	@EventHandler
	public void onTarget(EntityTargetEvent event) {
		if(event.getEntity().hasMetadata("morph")) { event.setCancelled(true); }
	}
	
}
