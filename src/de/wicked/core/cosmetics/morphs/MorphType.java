package de.wicked.core.cosmetics.morphs;

import org.bukkit.entity.EntityType;

public enum MorphType {

	CREEPER(EntityType.CREEPER, 50, "&aCreeper Morph"),
	SKELETON(EntityType.SKELETON, 51, "&aSkelett Morph"),
	SPIDER(EntityType.SPIDER, 52, "&aSpinnen Morph"),
	ZOMBIE(EntityType.ZOMBIE, 54, "&aZombie Morph"),
	SLIME(EntityType.SLIME, 55, "&aSchleim Morph"),
	ENDERMAN(EntityType.ENDERMAN, 58, "&aEnderman Morph"),
	PIG(EntityType.PIG, 90, "&aSchweine Morph"),
	SHEEP(EntityType.SHEEP, 91, "&aSchaf Morph"),
	COW(EntityType.COW, 92, "&aKuh Morph"),
	CHICKEN(EntityType.CHICKEN, 93, "&aHuhn Morph"),
	WOLF(EntityType.WOLF, 95, "&aWolf Morph"),
	VILLAGER(EntityType.VILLAGER, 120, "&aVillager Morph");
	
	private final int data;
	private final String name;
	private final EntityType type;
	
	private MorphType(EntityType type, int data, String name) {
		this.type = type;
		this.data = data;
		this.name = name;
	}
	
	public int data() { return data; }
	
	public String getName() { return name; }
	
	public EntityType getEntity() { return type; }
	
	public static MorphType getByData(int data) {
		for(MorphType t : values()) {
			if(t.data() == data) {
				return t;
			}
		}
		return null;
	}
}
