package de.wicked.core;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.profiles.ProfileSaver;
import de.wicked.core.utils.Config;

public class GameScheduler extends BukkitRunnable{

	int countdown = 3600;
	int daytimeTicks = 0;
	GregorianCalendar calendar;
	
	@Override
	public void run() {
		if(countdown <= 0) {
			int delay = 10;
			for(Player p : Bukkit.getOnlinePlayers()) {
				new ProfileSaver(WickedCore.getUser(p)).runTaskLaterAsynchronously(WickedCore.getInstance(),delay);
				delay+=2;
			}
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "say &8Profile gespeichert!");
			
			if(Config.SYNCTIME.getBoolean()) {
				calendar = new GregorianCalendar();
				int hours = calendar.get(Calendar.HOUR_OF_DAY);
				int ticks = (hours > 5 ? ((hours*1000)-6000):((hours*1000)+18000));
				
				Bukkit.getWorld(Config.LOBBYWORLD.getString()).setTime(ticks);
				WickedCore.debug("Echtzeitstunden: "+hours+" ("+ticks+" Ticks gesetzt)", true);
			}
			
			countdown = 3600;
		}
		
		countdown--;
	}

	public void push(int i) {
		countdown = i;
	}
	
}
