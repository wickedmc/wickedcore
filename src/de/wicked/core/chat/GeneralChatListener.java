package de.wicked.core.chat;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Utils;

public class GeneralChatListener implements Listener {
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		WickedUser user = WickedCore.getUser(player);
//		String message = event.getMessage();
		
		if(event.isCancelled()) {
			return;
		}
		
		//--------------------------------------------------------
		// Normaler Mute
		if(user.isMuted()) {
			if(!(player.hasPermission("wickedcore.chat.mute.bypass"))) {
				event.setCancelled(true);
				player.sendMessage(Utils.color("&cDu bist gemuted!"));
			}
		}
		
		String format;
		try {
			format = Config.CHATFORMAT.getString();
			format = format.replaceAll("%r", Utils.color(user.getRank().prefix()));
			format = format.replaceAll("%p", Utils.color(user.getActivePrefix()));
			format = format.replaceAll("%c", Utils.color(user.getRank().color()));
			format = format.replaceAll("%n", Utils.color(user.getName()));
			format = format.replaceAll("%d", Utils.color(user.getDisplayName()));
			format = format.replaceAll("%m", user.hasPermission(Rank.SUPPORTER) ? Utils.color(event.getMessage()):event.getMessage());
		} catch(Exception ex) { format = player.getName() + ": " + event.getMessage(); }
		
		event.setCancelled(true);
		Bukkit.broadcastMessage(format);
	}
	
}
