package de.wicked.core.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.Note.Tone;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AdminAlert implements Listener {

	@EventHandler(priority=EventPriority.NORMAL)
	public void onChat(AsyncPlayerChatEvent event) {
		
		// Checken, ob die Message mit ! startet und die Permission da ist
		if(event.getMessage().startsWith("!") && event.getPlayer().hasPermission("wickedcore.admin.alert")) {
			// Checken, ob es sich um eine !!! Nachricht handelt
			if(event.getMessage().replaceFirst("!", "").equalsIgnoreCase("")
					|| event.getMessage().replaceAll("!", "").equalsIgnoreCase("")) {
				return;
			}
			
			event.setCancelled(true);
			// Alert ausgeben
			Bukkit.broadcastMessage(
					ChatColor.translateAlternateColorCodes('&',
							"&4&l! &c" + event.getPlayer().getName()
							+ ": " + event.getMessage().replaceFirst("!", "")
							+ " &4&l!")
					);
			// Ton spielen
			for(Player p : Bukkit.getOnlinePlayers()) {
				p.playNote(p.getLocation(), Instrument.PIANO, Note.sharp(1, Tone.A));
			}
		}
		
	}
	
}
