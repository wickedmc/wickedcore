package de.wicked.core.commands;

import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandWicked implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) { Messages.NOPLAYER.send(sender); return true; }
		
		Player player = (Player) sender;
		WickedUser user = WickedCore.getUser(player);
		
		if(!user.hasPermission(Rank.ADMIN)) { Messages.NOPERM.send(player); return true; }
		
		if(args.length > 0) {
			switch(args[0]) {
			case "reload":
				
				try {
					Config.reload(true);
					Messages.reload(true);
				} catch (IOException e) {
					player.sendMessage(Utils.color("&cEtwas ist schief gelaufen :c")); return true;
				}
				
				player.sendMessage(Utils.color("&aConfig und Messages neu geladen.")); return true;
				
			case "info":
				player.sendMessage(Utils.color(""));
				player.sendMessage(Utils.color("&7<>=- &2&lWicked Info &7-=<>"));
				for(Plugin p : WickedCore.getInstalledWickedPlugins()) {
					player.sendMessage(Utils.color("&8> &a&n"+p.getName()+"&a:"));
					player.sendMessage(Utils.color("&8>>> &aVersion: &e"+p.getDescription().getVersion()));
					player.sendMessage(Utils.color(""));
				}
				break;
				
			case "vs":
				player.getInventory().clear();
				player.getInventory().setItem(0, new ItemStack(Material.WOOD_AXE));
				player.getInventory().setItem(1, new ItemStack(Material.COMPASS));
				player.getInventory().setItem(2, new ItemStack(Material.ARROW));
				player.getInventory().setItem(3, new ItemStack(Material.SULPHUR));
				break;
				
			case "stopthread":
				WickedCore.getGameScheduler().cancel();
				player.sendMessage(Utils.color("&cGameScheduler gestoppt!"));
				break;
				
			case "startthread":
				WickedCore.getGameScheduler().runTaskTimerAsynchronously(WickedCore.getInstance(), 10, 20);
				player.sendMessage(Utils.color("&cGameScheduler gestartet!"));
				break;
				
			case "pushthread":
				WickedCore.getGameScheduler().push(Integer.parseInt(args[1]));
				player.sendMessage(Utils.color("&cThread auf countdown="+args[1]+" gesetzt"));
				break;
				
			case "showdebug":
				user.setShowDebug(!user.isShowDebug());
				player.sendMessage(Utils.color("&7ShowDebug = "+user.isShowDebug()));
				break;
				
			case "gettime":
				player.sendMessage(Utils.color("&7Timeticks: "+player.getWorld().getTime()));
				break;
			
			default: break;
	
			}
		}
		
		return true;
	}
	
}
