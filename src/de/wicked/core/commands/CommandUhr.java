package de.wicked.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.utils.Utils;

public class CommandUhr implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("heartreset")) {
			Player p = (Player)sender;
			p.setMaxHealth(20);
			p.setHealth(20);
			p.setHealthScaled(false);
			p.sendMessage("Befehle ausgeführt:");
			p.sendMessage("player.setMaxHealth(20);");
			p.sendMessage("player.setHealth(20);");
			p.sendMessage("player.setHealthScaled(false);");
			return true;
		}
		
		sender.sendMessage(Utils.color("&bAktuelle Uhrzeit: &e" + Utils.milliesToDate(System.currentTimeMillis())));

		return true;
	}
	
}
