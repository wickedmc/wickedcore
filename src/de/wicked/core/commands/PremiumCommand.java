package de.wicked.core.commands;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Utils;

public class PremiumCommand implements CommandExecutor{

	Permission perms = WickedCore.getPermission();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(sender instanceof ConsoleCommandSender) {
			if(args.length == 1) {
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					sender.sendMessage(Utils.color("&cSpieler nicht gefunden!"));
					return true;
				}
				WickedCore.getUser(target).setRank(Rank.PREMIUM);
				Bukkit.dispatchCommand(sender, "pex user "+target.getName()+" group set Premium");
				sender.sendMessage(Utils.color("&aSpieler wurde zum Premium gesetzt"));
				return true;
			} else if(args.length == 2) {
				if(args[0].equalsIgnoreCase("-d")) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						sender.sendMessage(Utils.color("&cSpieler nicht gefunden!"));
						return true;
					}
					WickedCore.getUser(target).setRank(Rank.SPIELER);
					Bukkit.dispatchCommand(sender, "pex user "+target.getName()+" group set Spieler");
					sender.sendMessage(Utils.color("&aPremium wurde zum Spieler gesetzt"));
					return true;
				}
			} else {
				sender.sendMessage(Utils.color("&cSyntax: /setpremium <Spieler> [-d]"));
				return true;
			}
		}
		
		else if(sender instanceof Player) {
			
			Player player = (Player) sender;
			WickedUser user = WickedCore.getUser(player);
			
			if(!user.hasPermission(Rank.ADMIN)) {
				return true;
			}
			
			if(args.length == 1) {
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					player.sendMessage(Utils.color("&cSpieler nicht gefunden!"));
					return true;
				}
				WickedCore.getUser(target).setRank(Rank.PREMIUM);
				Bukkit.dispatchCommand(sender, "pex user "+target.getName()+" group set Premium");
				player.sendMessage(Utils.color("&aSpieler wurde zum Premium gesetzt"));
				return true;
			} else if(args.length == 2) {
				if(args[0].equalsIgnoreCase("-d")) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage(Utils.color("&cSpieler nicht gefunden!"));
						return true;
					}
					WickedCore.getUser(target).setRank(Rank.SPIELER);
					Bukkit.dispatchCommand(sender, "pex user "+target.getName()+" group set Spieler");
					player.sendMessage(Utils.color("&aSpieler wurde zum Premium gesetzt"));
					return true;
				}
			} else {
				player.sendMessage(Utils.color("&cSyntax: /setpremium <Spieler> [-d]"));
				return true;
			}
			
		}
		
		return true;
	}
	
}
