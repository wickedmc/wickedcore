package de.wicked.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;


public class CommandGeburtstag implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) {
			Messages.NOPLAYER.send(sender);
		}
		
		Player player = (Player) sender;
		
		// temp
		if(!WickedCore.getUser(player).hasPermission(Rank.ADMIN)) {
			player.sendMessage(Utils.color("&cDieser Befehl befindet sich noch in der Entwicklung!"));
			return true;
		}
		
		if(args.length == 0) {
			WickedCore.getMenuManager().getMenu("birthday").show(player);
			return true;
		} else if(args.length == 1 && args[0].equalsIgnoreCase("reset")) {
			// TODO resetten
			return true;
		}

		player.sendMessage(Utils.color("&6[Geburtstag] &aDu kannst hier deinen Geburtstag setzen"));
		player.sendMessage(Utils.color("&aund jedes Jahr tolle Geschenke bekommen!"));
		player.sendMessage(Utils.color("&6Befehle:"));
		player.sendMessage(Utils.color("&e/geburtstag &7- �ffnet das Menu"));
		player.sendMessage(Utils.color("&e/geburtstag reset &7- L�scht deinen Geburtstag wieder (&4&lEINMALIG!&7)"));
		
		return true;
	}
	
}
