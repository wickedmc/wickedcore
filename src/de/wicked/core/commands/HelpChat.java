package de.wicked.core.commands;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class HelpChat implements CommandExecutor, Listener{

//	private File file = new File("plugins/WickedCore", "suppchats.yml");
//	private YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	private Set<Player> playerQueue = new HashSet<>();
	private static HashMap<Player, Player> supportChats = new HashMap<>(); // Supporter, User
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		/*	Spieler -> /support [Grund]
		 * 	Nachricht an Team
		 *  Supporter -> /support accept <Spieler>
		 *  Chat Chat Chat
		 *  Spieler/Supporter -> /support close
		 *  
		 *  Zus�tzlich: /support spy
		 *  			/support list
		 *  			/support deny <Spieler>
		 *  			/support help
		 */
		
		if(!(sender instanceof Player)) {
			Messages.NOPLAYER.send(sender);
			return true;
		}
		
		Player player = (Player) sender;
		WickedUser user = WickedCore.getUser(player);
		
		if(!user.hasPermission(Rank.SUPPORTER)) {
			
			if(args.length >= 1 && args[0].equalsIgnoreCase("close")) {
				if(!(supportChats.containsValue(player))) {
					player.sendMessage(Utils.color("&cDu bist in keinem Chat!"));
					return true;
				}
				
				for(Player key : supportChats.keySet()) {
					if(supportChats.get(key) == player) {
						key.sendMessage(Utils.color("&e"+player.getName()+" &chat den Chat geschlossen!"));
						player.sendMessage(Utils.color("&cDu hast den Chat geschlossen!"));
						supportChats.remove(key);
						return true;
					}
				}
				player.sendMessage(Utils.color("&cDa ging etwas schief, das tut uns Leid!"));
				return true;
			}
			
			// Spieler -> Anfrage �ffnen
				if(playerQueue.contains(player) || supportChats.containsValue(player)) {
					player.sendMessage(Utils.color("&cDu bist bereits in der Support-Warteschlange!"));
					return true;
				}
			String grund = "";
				if(args.length > 0) {
					if(args[0].equalsIgnoreCase("help")) {
						// send help
					} else {
						for(int i = 0; i < args.length; i++) {
							grund += args[i]+" ";
						}
					}
				} else {
					grund = "kein Grund angegeben";
				}
			
			playerQueue.add(player);
			sendTeam("&6&lSUPP-CHAT &8\u2503 &a"+player.getName()+" &ebraucht Hilfe! [&c"+grund+"&e]");
			player.sendMessage(Utils.color("&aDu wurdest in die Warteschlange eingetragen. Bitte gedulde dich bis ein Teammitglied frei ist."));
			return true;
		} else {
			
			if(args.length == 0) {
				player.sendMessage(Utils.color("&c/support help"));
				return true;
			} else if(args.length == 1) {
				switch(args[0].toLowerCase()) {
				case "accept": player.sendMessage(Utils.color("&c/support accept <Spieler>")); return true;
				case "deny": player.sendMessage(Utils.color("&c/support deny <Spieler>")); return true;
				
				case "list":
					String list = "";
					if(playerQueue.isEmpty()) {
						list = "&c-leer-";
					} else {
						for(Player p : playerQueue) {
							list+="&7, &a"+p.getName();
						}
						list = list.replaceFirst("&7, ", "");
					}
					player.sendMessage(Utils.color("&6Aktuelle Warteschlange:"));
					player.sendMessage(Utils.color(list));
					return true;
				case "spy":
					if(!user.hasPermission(Rank.MODERATOR)) {
						player.sendMessage(Utils.color("&cDu darfst das nicht!"));
						return true;
					} else {
						boolean spy = user.isSupportChatSpy();
						user.setSupportChatSpy(!spy);
						player.sendMessage(Utils.color(!spy ?
								"&aDu liest nun alle Support-Chats" : "&cDu liest nichtmehr alle Support-Chats"));
						return true;
					}
				case "close":
					if(!supportChats.containsKey(player)) {
						player.sendMessage(Utils.color("&cDu bist nicht im SupportChat!"));
						return true;
					} else {
						sendTeam("&e"+player.getName()+" &chat den Chat mit &e"+supportChats.get(player).getName()+" &cgeschlossen!");
						supportChats.get(player).sendMessage(Utils.color("&e"+player.getName()+" &chat den Chat geschlossen!"));
						supportChats.remove(player);
						return true;
					}
				case "help":
					player.sendMessage(Utils.color("&8<>[==- &6/support oder /supp &8-==]<>"));
					player.sendMessage(Utils.color("&e/supp help &7- Zeigt diese Liste"));
					player.sendMessage(Utils.color("&e/supp list &7- Listet die aktuelle Warteschlange auf"));
					player.sendMessage(Utils.color("&e/supp accept <Spieler> &7- Akzeptiert eine Anfrage und �ffnet den Chat"));
					player.sendMessage(Utils.color("&e/supp deny <Spieler> &7- Lehnt eine Anfrage ab"));
					player.sendMessage(Utils.color("&e/supp close [Grund] &7- Schlie�t den aktiven Chat"));
					player.sendMessage(Utils.color("&e/supp spy &7- Zeigt alle Chats"));
					
					default:
					player.sendMessage(Utils.color("&c/support help"));
					return true;
				}
			} else if(args.length == 2) {
				String command = args[0].toLowerCase();
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					Messages.PLRNOTFOUND.send(sender);
					return true;
				}
				
				switch(command) {
				case "accept":
					
					if(!playerQueue.contains(target)) {
						player.sendMessage(Utils.color("&cDieser Spieler befindet sich nicht in der Warteschlange!"));
						return true;
					} else {
						sendTeam("&e"+player.getName()+" &6befindet sich nun im Chat mit &e"+target.getName());
						target.sendMessage(Utils.color("&6Du befindest dich nun im Chat mit &e"+player.getName()+" &6!"));
						supportChats.put(player, target);
//						Bukkit.broadcastMessage("In HashMap gesetzt: "+supportChats.get(player).getName());
//						Bukkit.broadcastMessage(supportChats.toString());
//						Bukkit.broadcastMessage("1: " + supportChats.containsKey(player) + ", 2: " + supportChats.containsValue(player));
//						Bukkit.broadcastMessage("1: " + contains(supportChats, player) + ", 2: " + contains(supportChats, player));
						playerQueue.remove(target);
						return true;
					}
					
				case "deny":
					if(!playerQueue.contains(target)) {
						player.sendMessage(Utils.color("&cDieser Spieler befindet sich nicht in der Warteschlange!"));
						return true;
					} else {
						sendTeam("&6"+player.getName()+" &chat &6"+target.getName()+"'s &cAnfrage abgelehnt!");
						target.sendMessage(Utils.color("&cDeine Anfrage wurde von &6"+player.getName()+" &cabgelehnt!"));
						playerQueue.remove(target);
						return true;
					}
					default: break;
				}
			}
		}
		return true;
	}
	
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		
//		HashMap<Integer, String> x = new HashMap<>();
//		x.put(1, "10");
//		x.put(2, "20");
//		Bukkit.broadcastMessage(supportChats.toString());
//		Bukkit.broadcastMessage("1: " + contains(x, 1) + ", \"20\": " + contains(x, "20"));
//		Bukkit.broadcastMessage("1: " + supportChats.containsKey(player) + ", 2: " + supportChats.containsValue(player));
//		Bukkit.broadcastMessage("1: " + contains(supportChats, player) + ", 2: " + contains(supportChats, player));
		
		if(supportChats.containsKey(player) || supportChats.containsValue(player)) {
			event.setCancelled(true);
			if(supportChats.containsKey(player)) {
				
				String message = Config.SUPPCHATFORMAT.getString().replaceAll("%player", player.getName())
									.replaceAll("%m", event.getMessage());
				String spyMessage = Config.SUPPCHATFORMAT.getString()
								.replaceAll("%player", player.getName()+" -> "+supportChats.get(player).getName())
								.replaceAll("%m", event.getMessage());
				
				
				player.sendMessage(Utils.color(message));
				supportChats.get(player).sendMessage(Utils.color(message));
				
				// Spy's senden
				for(WickedUser user : WickedCore.getUsers()) {
					if(user.isSupportChatSpy()) {
						user.getPlayer().sendMessage(Utils.color(spyMessage));
					}
				}

				log(Utils.stripColor(spyMessage));
				
			} else {
				for(Player key : supportChats.keySet()) {
					if(supportChats.get(key) == player) {

						String message = Config.SUPPCHATFORMAT.getString().replaceAll("%player", player.getName())
											.replaceAll("%m", event.getMessage());
						String spyMessage = Config.SUPPCHATFORMAT.getString()
										.replaceAll("%player", player.getName()+" -> "+key.getName())
										.replaceAll("%m", event.getMessage());
						
						player.sendMessage(Utils.color(message));
						key.sendMessage(Utils.color(message));
						
						// Spy's senden
						for(WickedUser user : WickedCore.getUsers()) {
							if(user.isSupportChatSpy()) {
								user.getPlayer().sendMessage(Utils.color(spyMessage));
							}
						}
						
						log(Utils.stripColor(spyMessage));
						
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if(supportChats.containsKey(player) || supportChats.containsValue(player)) {

			if(supportChats.containsKey(player)) {
				supportChats.get(player).sendMessage(Utils.color("&cDein Chatpartner hat die Verbindung geschlossen!"));
				supportChats.get(player).sendMessage(Utils.color("&cDer Chat wurde geschlossen."));	
				supportChats.remove(player);
				return;
			} else {
				for(Player key : supportChats.keySet()) {
					if(supportChats.get(key) == player) {
						key.sendMessage(Utils.color("&cDein Chatpartner hat die Verbindung geschlossen!"));
						key.sendMessage(Utils.color("&cDer Chat wurde geschlossen!"));
						supportChats.remove(key);
						return;
					}
				}
			}
		}
	}
	
	private void log(String msg) {
		File dataFolder = WickedCore.getInstance().getDataFolder();
		if(!dataFolder.exists()) {
			dataFolder.mkdir();
		}
	
		File logFile = new File(WickedCore.getInstance().getDataFolder(), "supportLog.txt");
		if(!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
		GregorianCalendar calendar = new GregorianCalendar();
		String timeStamp = "<"+calendar.get(Calendar.DAY_OF_MONTH)
				+". "+calendar.get(Calendar.MONTH)
				+". "+calendar.get(Calendar.HOUR_OF_DAY)
				+":"+calendar.get(Calendar.MINUTE)
				+":"+calendar.get(Calendar.SECOND)+">";
		FileWriter fw;
		PrintWriter pw;
		try {
			fw = new FileWriter(logFile, true);
			pw = new PrintWriter(fw);
			
			pw.println(timeStamp+" "+msg);
			pw.flush();
			pw.close();
		} catch (IOException e) { e.printStackTrace(); }

	}
	
	private void sendTeam(String msg) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(WickedCore.getUser(p).hasPermission(Rank.SUPPORTER)) {
				p.sendMessage(Utils.color(msg));
			}
		}
	}
}
