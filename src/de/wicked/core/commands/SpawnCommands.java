package de.wicked.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.PlayerMode;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;
import de.wicked.core.utils.menus.NavigationMenu;

public class SpawnCommands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) {
			Messages.NOPLAYER.send(sender);
			return true;
		}
		
		Player player = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("lobby")) {
			
			player.teleport(NavigationMenu.getLoc("spawn"));
			WickedCore.getUser(player).setPlayerMode(PlayerMode.LOBBY);
			Utils.setLobby(player);
			player.sendMessage(Utils.color("&eWillkommen zur�ck in der Lobby!"));
			
		} else if(cmd.getName().equalsIgnoreCase("spawn")) {
			
			player.teleport(player.getWorld().getSpawnLocation());
			player.sendMessage(Utils.color("&aDu wirst zum Spawn teleportiert."));
			player.sendMessage(Utils.color("&aNutze /lobby um zur�ck zur Lobby zu kommen."));
			return true;
		}
		
		
		
		return true;
	}
	
}
