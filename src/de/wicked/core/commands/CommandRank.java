package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Utils;

public class CommandRank implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		
		if(sender instanceof Player) {
			sender.sendMessage(Utils.color("&cDieser Befehl ist nurnoch �ber die Konsole ausf�hrbar!"));
		} else if(sender instanceof ConsoleCommandSender) {
			
			if(args.length != 2) {
				sender.sendMessage(Utils.color("&cSyntax: /rank RANK player"));
				return true;
			}
			
			Rank rank;
			try {
				rank = Rank.valueOf(args[0].toUpperCase());
			} catch(Exception ex) {
				sender.sendMessage(Utils.color("&cDas ist kein vorhandener Rang")); return true;
			}
			Player target = Bukkit.getPlayer(args[1]);
			
			if(target == null) { sender.sendMessage(Utils.color("&cSpieler ist nicht online")); return true; }
			
			WickedCore.getUser(target).setRank(rank);
			sender.sendMessage(target.getName() + " Rang "+ rank.name() + " gegeben.");
			
			return true;
		}
		
		
		
		return true;
	}
	
}
