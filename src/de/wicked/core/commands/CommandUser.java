package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandUser implements CommandExecutor{

	// Mit diesem Befehl (/user <Spieler>) k�nnen s�mtliche Infos �ber einen WickedUser angezeigt werden.

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(!WickedCore.getUser((Player)sender).hasPermission(Rank.MODERATOR)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		
		if(args.length == 1) {
			
			Player player = (Player) sender;
			
			String name = args[0];
			Player target = Bukkit.getPlayer(name);
			WickedUser user;
			
			if(target == null) {
				
				// todo
				
				
			} else {
				user = WickedCore.getUser(target);
				
				player.sendMessage(Utils.color(
						"&b\u2580\u2580\u2580\u2580\u2580 &bUser-Info &b\u2580\u2580\u2580\u2580\u2580"));
				player.sendMessage(Utils.color("&7Name: &e" + user.getName()));
				sendRawMsg(player, "{text:\""
									+ Utils.color("&8&l[&7Player Info&8&l]")
									+ "\",hoverEvent:{action:\"show_text\",value:\""
									+ Utils.color("&7Name: &e" + user.getName()
									+ "\n&7UUID: &e" + user.getUuidString())
									+ "\"}}");
//				sendRawMsg(player, "{text:\""
//									+ Utils.color("&8&l[&7Last Location&8&l]")
//									+ "\",hoverEvent:{action:\"show_text\",value:\""
//									+ Utils.color("&7World: &e" + user.getLastLoc().getWorld().getName()
//									+ "\n&7X: &e" + user.getLastLoc().getX()
//									+ "\n&7Y: &e" + user.getLastLoc().getY()
//									+ "\n&7Z: &e" + user.getLastLoc().getZ())
//									+ "\"}}");
				sendRawMsg(player, "{text:\""
						+ Utils.color("&8&l[&7User Info&8&l]")
						+ "\",hoverEvent:{action:\"show_text\",value:\""
						+ Utils.color("&7Nick: &e" + user.getNick()
						+ "\n&7Prefix: &e" + user.getPrefix()
						+ "\n&7Rank: &e" + user.getRank().prefix()
						+ "\n&7Mode: &e" + user.getPlayerMode().name())
						+ "\"}}");
				sendRawMsg(player, "{text:\""
						+ Utils.color("&8&l[&7Stats&8&l]")
						+ "\",hoverEvent:{action:\"show_text\",value:\""
						+ Utils.color("&7Kills: &e" + user.getKills()
						+ "\n&7Deaths: &e" + user.getDeaths()
						+ "\n&7Coins: &e" + user.getCoins())
						+ "\"}}");
				sendRawMsg(player, "{text:\""
						+ Utils.color("&8&l[&7Logins&8&l]")
						+ "\",hoverEvent:{action:\"show_text\",value:\""
						+ Utils.color("&7First Login: &e" + Utils.milliesToDate(user.getFirstLogin())
						+ "\n&7Last Login: &e" + Utils.milliesToDate(user.getLastLogin())
						+ "\n&7Last Logout: &e" + Utils.milliesToDate(user.getLastLogout())
						+ "\n&7On Time: &e" + Utils.milliesToString(user.getOnTime())
						+ "\n&7Online seit: &a" + Utils.milliesToString(user.getLastLogin(), System.currentTimeMillis()))
						+ "\"}}");
				sendRawMsg(player, "{text:\""
						+ Utils.color("&8&l[&7Baninfo&8&l]")
						+ "\",hoverEvent:{action:\"show_text\",value:\""
						+ Utils.color("&7Gebannt: &e" + (WickedCore.getBanManager().isBanned(user.getUuidString()) ? "&aja"
						+ "\n&7Gebannt f�r: &e" + WickedCore.getBanManager().getReason(user.getUuidString())
						+ "\n&7Gebannt von: &e" + WickedCore.getBanManager().getAuthor(user.getUuidString())
						+ "\n&7Gebannt bis: &e" + Utils.milliesToString(WickedCore.getBanManager().getEnd(user.getUuidString()))
						+ "\n&7Gebannt seit: &a" + Utils.milliesToString(user.getLastLogin(), System.currentTimeMillis()):"&cnein"))
						+ "\"}}");
				sendRawMsg(player, "{text:\""
							+ Utils.color("&8&l[&7SaveInv&8&l]")
							+ "\",clickEvent:{action:\"run_command\",value:\""
							+ "/osi " + user.getName() + "\"},hoverEvent:{action:\"show_text\",value:\""
							+ Utils.color("&7�ffnen")
							+ "\"}}" );
				
				player.sendMessage(Utils.color(
						"&b\u2580\u2580\u2580\u2580\u2580 &bUser-Info &b\u2580\u2580\u2580\u2580\u2580"));
				
			}
			
			return true;
			
		} else if(args.length == 2) {
			
		}
		
		sender.sendMessage(Utils.color("&cSyntax: /user <Spieler>"));
		
		return true;
	}
	
	private void sendRawMsg(Player p, String msg) {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + p.getName() + " " + msg);
	}
}
