package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandOpensaveinv implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) {
			Messages.NOPLAYER.send(sender);
			return true;
		}
		
		Player player = (Player) sender;
		WickedUser user = WickedCore.getUser(player);
		
		if(!(user.hasPermission(Rank.MODERATOR))) {
			Messages.NOPERM.send(player);
			return true;
		}
		
		if(args.length == 1) {
			
			Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				Messages.PLRNOTFOUND.send(player);
				return true;
			}
			WickedUser targetUser = WickedCore.getUser(target);
			Inventory inv = Bukkit.getServer().createInventory(player, 45, Utils.color("&cSaveInv von: &6" + target.getName()));
			inv.setContents(targetUser.getLastInv().getContents());
			player.openInventory(inv);
			
			return true;
		}
		
		player.sendMessage(Utils.color("&cSyntax: /opensaveinv <Spieler>"));
		
		return true;
	}
	
}
