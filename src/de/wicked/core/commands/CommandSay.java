package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.utils.Utils;

public class CommandSay implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		
		if(sender instanceof ConsoleCommandSender) {
				
			String message = "";
			for(String s : args) {
				message += s + " ";
			}
				
			Bukkit.broadcastMessage(Utils.color("&6\u25C4[ Server ]\u25BA &e" + message));
			return true;
		} else if(sender instanceof Player) {
			
			if(!(sender.hasPermission("wicked.core.say"))) {
				return true;
			}
			
			String message = "";
			for(String s : args) {
				message += s + " ";
			}
				
			Bukkit.broadcastMessage(Utils.color(message));
			return true;
		}
		else {
			return true;
		}
	}
}
