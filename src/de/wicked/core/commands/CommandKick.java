package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandKick implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(sender instanceof Player) {
			if(!(WickedCore.getUser((Player)sender).hasPermission(Rank.BUILDER))) {
				Messages.NOPERM.send(sender);
				return true;
			}
			
			if(args.length == 0 || Bukkit.getPlayer(args[0]) == null) {
				sender.sendMessage(Utils.color("&c/kick <Spieler>"));
				return true;
			}
			
			if(WickedCore.getUser(Bukkit.getPlayer(args[0])).hasPermission(WickedCore.getUser((Player)sender).getRank())) {
				sender.sendMessage(Utils.color("&cDu kannst diesen Spieler nicht kicken!")); return true;
			}
		}
		
		Player target = Bukkit.getPlayer(args[0]);
		if(target == null) {
			Messages.PLRNOTFOUND.send(sender);
			return true;
		}
		String rsn = "";
		if(args.length > 1) {
			for(int i = 1; i < args.length; i++) {
				rsn += i+" ";
			}
		} else {
			rsn = "Gekickt!";
		}
		
		target.kickPlayer(Utils.color("&cDu wurdest gekickt!\n\n&e"+rsn));
		
		Utils.broadcastAll("&6Wicked &8\u2503 &a"+target.getName()+" &7wurde gekickt!");
		Utils.broadcastAll("&6Wicked &8\u2503 &7gekickt von: "+sender.getName());
		Utils.broadcastAll("&6Wicked &8\u2503 &7gekickt wegen: "+rsn);
		
		
		
		return true;
	}
	
}
