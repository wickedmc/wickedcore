package de.wicked.core.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandPrefix implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player)) {
			Messages.NOPLAYER.send(sender);
			return true;
		}
		
		Player player = (Player) sender;
		WickedUser user = WickedCore.getUser(player);
		
		if(!(user.hasPermission(Rank.PREMIUM))) {
			Messages.NOPERM.send(user.getPlayer());
			return true;
		}
		
		if(args.length == 1) {
			
			switch(args[0]) {
			case "color":
				player.sendMessage("�7Nutze Farbcodes mit & + Code (zB &8)");
				player.sendMessage(Utils.color("&7M�gliche Codes: " + legalCodes()));
				return true;
			case "off":
				user.setPrefix("");
				player.sendMessage(Utils.color("&6Prefix zur�ckgesetzt"));
				return true;
				default:
					if(checkIllegalCharacters(args[0])) {
						user.setPrefix(args[0]);
						player.sendMessage(Utils.color("&6Dein Prefix ist jetzt &r" + args[0]));
						return true;
					}
					
					player.sendMessage(Utils.color("&cDein Prefix enth�lt verbotene Worte oder Codes, bitte such dir ein anderes aus."));
			}
			
			return true;
		}
		
		sendHelp(user.getPlayer());
		
		return true;
	}
	
	private void sendHelp(Player p) {
		p.sendMessage(Utils.color("&6/prefix <Prefix> &7- Setzt dein Prefix"));
		p.sendMessage(Utils.color("&6/prefix colors &7- Zeigt benutzbare Farbcodes"));
		p.sendMessage(Utils.color("&6/prefix off &7- Setzt dein Prefix zur�ck."));
	}
	
	private boolean checkIllegalCharacters(String s) {
//		String[] blacklist = {"&2","&4","&5","&a","&c","&d","&k","&l","&n",
//				"Gott","Admin","Supporter","Besitzer","Owner","Moderator","Builder","Supporter",
//				"Griefer","Hurensohn","Arschloch","YouTuber"};
		List<String> blacklist = Config.PREFIX_BLACKLIST.getStringList();
		
		for(String c : blacklist) {
			if(s.contains(c)){
				return false;
			}
		}
		return true;
	}
	
	private String legalCodes() {
		String allCodes = "&00&11&22&33&44&55&66&77&88&99&aa&bb&cc&dd&ee&ff";
		String legalCodes = allCodes;
		List<String> blacklist = Config.PREFIX_BLACKLIST.getStringList();
		
		for(String s : blacklist) {
			legalCodes = legalCodes.replace(s, "");
		}
		
		return legalCodes;
	}
}
