package de.wicked.core.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.mysql.MySQL;
import de.wicked.core.utils.Utils;

public class CommandSetinfo implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender.getName().equalsIgnoreCase("Didi_Skywalker") || sender.getName().equals("DanCr4fter"))) {
			return true;
		}
		
		if(args[0].equalsIgnoreCase("nav")) {
			MySQL mysql = WickedCore.getMySQL();
			Location loc = ((Player)sender).getLocation();
			double x = loc.getX(); double y = loc.getY(); double z = loc.getZ();
			float pitch = loc.getPitch(); float yaw = loc.getYaw();
			String world = loc.getWorld().getName();
			
			try {
			mysql.update("INSERT INTO SpawnInfo(Name,World,X,Y,Z,Pitch,Yaw) VALUES('"+args[1]+"','"+world+"','"+x+"','"+y+"','"+z+"','"+pitch+"','"+yaw+"')");
	
			sender.sendMessage(Utils.color("&cInfo &e"+args[1]+" &cgesetzt"));
			} catch(Exception ex) {
				sender.sendMessage(Utils.color("&cFehler."));
				ex.printStackTrace();
			}
			return true;
		}
		
		return true;
	}
	
}
