package de.wicked.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.ProfileLoader;
import de.wicked.core.profiles.ProfileSaver;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Utils;

public class CommandUsers implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(sender instanceof Player) {
			if(!WickedCore.getUser(((Player)sender)).hasPermission(Rank.ADMIN)) {
				return true;
			}
		}
		
		
		if(args.length == 0) {
			sender.sendMessage(Utils.color("&cSyntax: /users save/load"));
		}
		
		switch(args[0]) {
		case "save":
			int later = 0;
			for(Player p : Bukkit.getOnlinePlayers()) {
				new BukkitRunnable() {
					@Override
					public void run() {
						new ProfileSaver(WickedCore.getUser(p)).runTaskAsynchronously(WickedCore.getInstance());
						sender.sendMessage(Utils.color("&7(Spieler: "+p.getName()+" gespeichert.)"));
					}
				}.runTaskLaterAsynchronously(WickedCore.getInstance(), later+2);
			}
			return true;
		case "load":
			int later2 = 0;
			for(Player p : Bukkit.getOnlinePlayers()) {
				new BukkitRunnable() {
					@Override
					public void run() {
						new ProfileLoader(WickedCore.getUser(p)).runTaskAsynchronously(WickedCore.getInstance());
						sender.sendMessage(Utils.color("&7(Spieler: "+p.getName()+" gespeichert.)"));
					}
				}.runTaskLaterAsynchronously(WickedCore.getInstance(), later2+2);
			}
			return true;
		}
		
		
		return true;
	}
	
}
