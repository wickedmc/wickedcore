package de.wicked.core.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.PlayerMode;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.Utils;

public class CommandMode implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		WickedUser user = WickedCore.getUser((Player) sender);
		
		if(!(user.hasPermission(Rank.BUILDER))) {
			return true;
		}
		
		if(args.length == 0) {
			user.getPlayer().sendMessage(Utils.color("&bModus: &e" + user.getPlayerMode().name()));
			return true;
		}
		
		if(user.getPlayer().getWorld().getName().equalsIgnoreCase(Config.LOBBYWORLD.getString())) {
			if(!user.hasPermission(Rank.MODERATOR)) {
				user.getPlayer().sendMessage(Utils.color("&cDu darfst deinen Modus in der Lobby nich �ndern!"));
				return true;
			}
		}
		
		try {
			user.setPlayerMode(PlayerMode.valueOf(args[0].toUpperCase()));
			user.getPlayer().setGameMode(PlayerMode.valueOf(args[0].toUpperCase()).getGameMode());
		} catch(Exception ex) {
			sender.sendMessage(Utils.color("&cFehler."));
		}
		user.getPlayer().sendMessage(Utils.color("&bModus: &e" + args[0]));
		
		return true;
	}
	
}
