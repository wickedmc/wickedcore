package de.wicked.core.bans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.mysql.MySQL;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Utils;

public class BanManager {

	private MySQL mysql = WickedCore.getMySQL();
	
	public void ban(String name, String uuid, long secs, String reason, String author) {
		long end;
		if(secs == -1) {
			end = -1;
		} else {
			long current = System.currentTimeMillis();
			long millis = secs*1000;
			end = current+millis;
		}
		
		final String INSERT = "INSERT INTO BannedPlayers(UUID,Name,Anfang,Ende,Grund,Author) VALUES(?,?,?,?,?,?) ON DUPLICATE KEY UPDATE Ende=?;";
		
		try {
			PreparedStatement ps = mysql.getCurrentConnection().prepareStatement(INSERT);
			ps.setString(1, uuid);
			ps.setString(2, name);
			ps.setLong(3, System.currentTimeMillis());
			ps.setLong(4, end);
			ps.setString(5, reason);
			ps.setString(6, author);
			ps.setLong(7, end);
			ps.execute();
			ps.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		if(Bukkit.getPlayer(name) != null) {
			// Spieler online
			Player target = Bukkit.getPlayer(name);
			if(WickedCore.getUser(target).hasPermission(Rank.ADMIN)) {
				return;
			}
			
			target.kickPlayer(Utils.color("&c&lDu wurdest gebannt!\n\n&9&nGrund:\n&b"+reason
				+ "\n\n&9&nVerbleibende Zeit:\n&b" + getRemainingTime(uuid)));
			
			
			Utils.broadcastAll("&6Wicked &8\u2503 &a" + target.getName() + " &7wurde gebannt");
			Utils.broadcastAll("&6Wicked &8\u2503 &7Gebannt f�r: &a" + reason);
			Utils.broadcastAll("&6Wicked &8\u2503 &7Gebannt von: &a" + author);
			Utils.broadcastAll("&6Wicked &8\u2503 &7Zeitraum: &a" + getRemainingTime(uuid));
		} else {
			// Spieler offline
			Utils.broadcastAll("&6Wicked &8\u2503 &a" + name + " &7wurde gebannt");
			Utils.broadcastAll("&6Wicked &8\u2503 &7Gebannt f�r: &a" + reason);
			Utils.broadcastAll("&6Wicked &8\u2503 &7Gebannt von: &a" + author);
			Utils.broadcastAll("&6Wicked &8\u2503 &7Zeitraum: &a" + getRemainingTime(uuid));
		}
		
		WickedCore.debug("[BAN] <"+name+"> gebannt von <"+author+"> wegen <"+reason+"> bis <"+getRemainingTime(uuid)+">", true);
	}
	
	public void unban(String uuid) {
		mysql.update("DELETE FROM BannedPlayers WHERE UUID='"+uuid+"'");
	}
	
	public boolean isBanned(String uuid) {
		ResultSet rs = mysql.getResult("SELECT Ende FROM BannedPlayers WHERE UUID='"+uuid+"'");
		
		try {
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
	}
	
	public String getReason(String uuid) {
		ResultSet rs = mysql.getResult("SELECT Grund FROM BannedPlayers WHERE UUID='"+uuid+"'");
		try {
			while(rs.next()) {
				return rs.getString("Grund");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		return "";
	}
	
	public Long getEnd(String uuid) {
		ResultSet rs = mysql.getResult("SELECT Ende FROM BannedPlayers WHERE UUID='"+uuid+"'");
		try {
			while(rs.next()) {
				return rs.getLong("Ende");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		return null;
	}
	
	public Long getStart(String uuid) {
		ResultSet rs = mysql.getResult("SELECT Anfang FROM BannedPlayers WHERE UUID='"+uuid+"'");
		try {
			while(rs.next()) {
				return rs.getLong("Anfang");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		return null;
	}
	
	public List<String> getBannedPlayers() {
		List<String> list = new ArrayList<String>();
		ResultSet rs = mysql.getResult("SELECT * FROM BannedPlayers");
		
		try {
			while(rs.next()) {
				list.add(rs.getString("Name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		
		return list;
	}
	
	public String getAuthor(String uuid) {
		ResultSet rs = mysql.getResult("SELECT Author FROM BannedPlayers WHERE UUID='"+uuid+"'");
		try {
			while(rs.next()) {
				return rs.getString("Author");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		return "";
	}
	
	public String getRemainingTime(String uuid) {
		long current = System.currentTimeMillis();
		long end = getEnd(uuid);
		if(end == -1) {
			return "permanent";
		}
		long difference = end-current; //millisekunden
		int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
		
		while(difference > 1000) { difference -= 1000; secs++; }
		while(secs > 60) { secs -= 60; mins++; }
		while(mins > 60) { mins -= 60; hours++; }
		while(hours > 24) { hours -= 24; days++; }
		while(days > 7) { days -= 7; weeks++; }
		while(weeks > 52) { weeks -= 52; years++; }
		
		return ((years > 0) ? ((years > 1) ? years+" Jahre, ":years+" Jahr, "):"")
				+ ((weeks > 0) ? ((weeks > 1) ? weeks+" Wochen, ":weeks+" Woche, "):"")
				+ ((days > 0) ? ((days > 1) ? days+" Tage, ":days+" Tag, "):"")
				+ ((hours > 0) ? ((hours > 1) ? hours+" Stunden, ":hours+" Stunde, "):"")
				+ ((mins > 0) ? ((mins > 1) ? mins+" Minuten, ":mins+" Minute, "):"")
				+ ((secs > 0) ? ((secs > 1) ? secs+" Sekunden":secs+" Sekunde"):"");
	}
	
}
