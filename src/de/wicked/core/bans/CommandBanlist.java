package de.wicked.core.bans;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandBanlist implements CommandExecutor{

	private BanManager bmanager = new BanManager();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		
		if(!(sender instanceof Player || sender instanceof ConsoleCommandSender)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(sender instanceof Player && !WickedCore.getUser((Player)sender).hasPermission(Rank.SUPPORTER)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		if(args.length == 0) {
			
			StringBuilder list = new StringBuilder();
			List<String> bannedP = bmanager.getBannedPlayers();
			for(int i = 0; i < bannedP.size(); i++) {
				if(i == bannedP.size()-1) {
					list.append("&f"+bannedP.get(i));
				} else {
					list.append("&f"+bannedP.get(i)+"&8, ");
				}
			}
			
			sender.sendMessage(Utils.color("&6Aktuell gebannte Spieler &c[&e" + (bannedP.size()) + "&c]&6:"));
			sender.sendMessage(Utils.color(list.toString()));
			
			return true;
		}
		
		sender.sendMessage(Utils.color("&cSyntax: /banlist"));
		sender.sendMessage(Utils.color("&cUm Infos �ber einen Ban anzuzeigen: /baninfo <Spieler>"));
		
		return true;
	}
	
}
