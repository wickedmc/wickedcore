package de.wicked.core.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

@SuppressWarnings("deprecation")
public class CommandBan implements CommandExecutor{

	private BanManager bmanager = new BanManager();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player || sender instanceof ConsoleCommandSender)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(sender instanceof Player && !WickedCore.getUser((Player)sender).hasPermission(Rank.MODERATOR)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		if(args.length >= 2) {
			String name = args[0];
			
			String reason = "";
				for(int i = 1; i < args.length; i++) {
					reason += args[i]+" ";
				}
			bmanager.ban(name, uuid(name), -1, reason, sender.getName());
			
			return true;
		}
		
		sender.sendMessage(Utils.color("&cSyntax: /ban <Spieler> <Grund>"));
		
		return true;
	}
	
	private String uuid(String name) {
		return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
	}
	
}
