package de.wicked.core.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandUnban implements CommandExecutor{

	private BanManager bmanager = new BanManager();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {

		if(!(sender instanceof Player || sender instanceof ConsoleCommandSender)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(sender instanceof Player && !WickedCore.getUser((Player)sender).hasPermission(Rank.SUPPORTER)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		if(args.length == 1) {
			
			String name = args[0];
			
			bmanager.unban(uuid(name));
			Utils.broadcastTeam("&6Wicked &8\u00BB &a"+name+" &7wurde entbannt!");
			
			return true;
		}
		
		sender.sendMessage(Utils.color("&cSyntax: /unban <Spieler>"));
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private String uuid(String name) {
		return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
	}
}
