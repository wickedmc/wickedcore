package de.wicked.core.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandBaninfo implements CommandExecutor{

	private BanManager bmanager = new BanManager();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		

		if(!(sender instanceof Player || sender instanceof ConsoleCommandSender)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(sender instanceof Player && !WickedCore.getUser((Player)sender).hasPermission(Rank.SUPPORTER)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		if(args.length == 1) {
			
			String name = args[0];
			
			if(bmanager.isBanned(uuid(name))) {
			
				String grund = bmanager.getReason(uuid(name));
				String author = bmanager.getAuthor(uuid(name));
				String anfang = Utils.milliesToDate(bmanager.getStart(uuid(name)));
				String ende = Utils.milliesToDate(bmanager.getEnd(uuid(name)));
				String zeit = bmanager.getRemainingTime(uuid(name));
				
				sender.sendMessage(Utils.color("&c\u2580\u2580\u2580\u2580\u2580 &6Ban-Info &c\u2580\u2580\u2580\u2580\u2580"));
				
				sender.sendMessage(Utils.color("&cName: &e" + name));
				sender.sendMessage(Utils.color("&cGebannt f�r: &e" + grund));
				sender.sendMessage(Utils.color("&cGebannt von: &e" + author));
				sender.sendMessage(Utils.color("&cGebannt seit: &e" + anfang));
				sender.sendMessage(Utils.color("&cGebannt bis: &e" + ende));
				sender.sendMessage(Utils.color("&cVerbleibende Zeit: &e" + zeit));

				sender.sendMessage(Utils.color("&c\u2580\u2580\u2580\u2580\u2580 &6Ban-Info &c\u2580\u2580\u2580\u2580\u2580"));
				
				
				return true;
			}
			
			sender.sendMessage(Utils.color("&cDieser Spieler ist nicht gebannt."));
			sender.sendMessage(Utils.color("&cNutze /banlist f�r eine Liste der gebannten Spieler."));
			
			return true;
		}
		
		sender.sendMessage(Utils.color("&cSyntax: /baninfo <Spieler>"));
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private String uuid(String name) {
		return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
	}
}
