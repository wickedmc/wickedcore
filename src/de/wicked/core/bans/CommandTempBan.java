package de.wicked.core.bans;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.Rank;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;

public class CommandTempBan implements CommandExecutor{

	private BanManager bmanager = new BanManager();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lbl,
			String[] args) {
		
// Format: 1y2w3d4h5m6s
// Syntax: /tempban <spieler> <zeit> <grund>
		
		if(!(sender instanceof Player || sender instanceof ConsoleCommandSender)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		if(sender instanceof Player && !WickedCore.getUser((Player)sender).hasPermission(Rank.SUPPORTER)) {
			Messages.NOPERM.send(sender);
			return true;
		}
		
		if(args.length >= 3) {
			String name = args[0];
			
			// Grund
			String grund = "";
			for(int i = 2; i < args.length; i++) {
				grund += args[i] + " ";
			}
			
			// Zeit festlegen
			int seconds;
			char[] zeitInput = args[1].toCharArray();
			String zeitChache = "";
			int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
			for(char c : zeitInput) {
				if(isNumber(c)) {
					zeitChache += c;
					continue;
				} else {
					switch(c) {
					case 'y': years = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					case 'w': weeks = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					case 'd': days = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					case 'h': hours = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					case 'm': mins = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					case 's': secs = Integer.parseInt(zeitChache); zeitChache = ""; continue;
						default:
							sender.sendMessage(Utils.color("&cFehlerhafte Zeitangabe. Erlaubte Charaktere:"));
							sender.sendMessage(Utils.color("&c0-9,y,w,d,h,m,s"));
							return true;
					}
				}
			}
			seconds = secs+(mins*60)+(hours*3600)+(days*24*3600)+(weeks*7*24*3600)+(years*52*7*24*3600);
			
			// Bannen
			bmanager.ban(name, uuid(name), seconds, grund, sender.getName());
			return true;
		}
		sender.sendMessage(Utils.color("&cSyntax: /ban <Spieler> <Zeit> <Grund>"));
		
		return true;
	}
	
	@SuppressWarnings("deprecation")
	private String uuid(String name) {
		return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
	}
	
	private boolean isNumber(char ch) {
		char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};
		for(char c : numbers) {
			if(c == ch) {
				return true;
			}
		}
		return false;
	}
}
