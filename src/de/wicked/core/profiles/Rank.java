package de.wicked.core.profiles;

public enum Rank {
	
	SPIELER(0, "&7Spieler", "&f", 0),
	YOUTUBER(1, "&fYou&4Tuber", "&f", 10),
	PREMIUM(1, "&6Premium", "&e", 10),
	VIP(2, "&bVIP", "", 15),
	BUILDER(3, "&eBuilder", "&e", 30),
	DEVELOPER(4, "&3Developer", "&b", 40),
	SUPPORTER(5, "&2Supporter", "&a", 50),
	MODERATOR(6, "&5Moderator", "&d", 80),
	ADMIN(7, "&4Admin", "&c", 100),
	OWNER(8, "&4Owner", "&c", 100);
	
	private final int id;
	private final String prefix;
	private final String color;
	private final int permission;
	
	private Rank(int id, String prefix, String color, int permission) {
		this.id = id;
		this.prefix = prefix;
		this.color = color;
		this.permission = permission;
	}
	
	public int id() {
		return this.id;
	}
	
	public String prefix() {
		return this.prefix;
	}
	
	public String color() {
		return this.color;
	}
	
	public int permission() {
		return this.permission;
	}
	
	

}
