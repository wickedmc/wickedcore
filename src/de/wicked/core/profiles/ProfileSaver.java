package de.wicked.core.profiles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.mysql.MySQL;

public class ProfileSaver extends BukkitRunnable{

	private WickedUser user;
	private MySQL mysql;
	
	private static final String SAVE = "UPDATE `UserData` SET Name=?,PlayerMode=?,Rank=?,Prefix=?,Nick=?, "
									+ "activeGadget=?,lastGadgetUse=?,lastMorphUse=?,firstLogin=?,lastLogin=?,lastLogout=?, "
									+ "lastX=?, lastY=?, lastZ=?, "
									+ "OnTime=?,muted=?,muteExpire=?,Kills=?,Deaths=?,Coins=?, supportChatSpy=?, showDebug=?"
									+ " WHERE uuid=?";
	
	public ProfileSaver(WickedUser user) {
		this.user = user;
		mysql = WickedCore.getMySQL();
	}
	
	@Override
	public void run() {
		Connection connection = null;
		
		try {
			connection = mysql.getCurrentConnection();
			
			PreparedStatement ps = connection.prepareStatement(SAVE);
			ps.setString(1, user.getName()); 								// Name
			ps.setString(2, user.getPlayerMode().name());					// PlayerMode
			ps.setString(3, user.getRank().name());							// Rank
			ps.setString(4, user.getPrefix());								// Prefix
			ps.setString(5, user.getNick());								// Nick
			ps.setString(6, user.getActiveGadget().getName());				// ActiveGadget
			ps.setLong(7, user.getLastGadgetUse());							// LastGadgetUse
			ps.setLong(8, user.getLastMorphUse());							// LastMorphUse
			ps.setLong(9, user.getFirstLogin());							// FirstLogin
			ps.setLong(10, user.getLastLogin());							// LastLogin
			ps.setLong(11, System.currentTimeMillis());						// LastLogout
			ps.setDouble(12, user.getLastLoc().getX());						// LastX
			ps.setDouble(13, user.getLastLoc().getY());						// LastY
			ps.setDouble(14, user.getLastLoc().getZ());						// LastZ
			ps.setLong(15, /*user.getOnTime()*/0);							// OnTime
			ps.setBoolean(16, user.isMuted());								// Muted
			ps.setLong(17, user.getMuteExpire());							// MuteExpire
			ps.setInt(18, user.getKills());									// Kills
			ps.setInt(19, user.getDeaths());								// Deaths
			ps.setInt(20, user.getCoins());									// Coins
			ps.setBoolean(21, user.isSupportChatSpy());						// SupportChatSpy
			ps.setBoolean(22, user.isShowDebug());							// ShowDebug
			ps.setString(23, user.getUuidString());							// UUID
			ps.execute();
			
			// Geburtstag
			ps = connection.prepareStatement("UPDATE UserBirthdays SET reset=?, date=? WHERE uuid=?");
			ps.setBoolean(1, user.isBirthdayReset());
			ps.setString(2, user.getBirthday().toString());
			ps.setString(3, user.getUuidString());
			ps.execute();
			
			ps.close();

			WickedCore.debug("Profil " + user.getName() + " gespeichert.", true);
			
		} catch(SQLException ex) {
			ex.printStackTrace();
			WickedCore.debug("Fehler beim speichern von: " + user.getName(), true);
		} finally {
			if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
		}
		
	}
}
