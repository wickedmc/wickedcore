package de.wicked.core.profiles;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.wicked.core.cosmetics.gadgets.Gadget;
import de.wicked.core.cosmetics.morphs.Morph;
import de.wicked.core.utils.Birthday;

public class WickedUser {

	private UUID uuid;
	private String name;
	private PlayerMode playerMode;
	private Rank rank;
	private String prefix;
	private String nick;
	
	private int coins;
	private int kills;
	private int deaths;
	
	private long firstLogin;
	private long lastLogin;
	private long lastLogout;
	private long onTime;
	private long premiumTestStart;
	
	private Gadget activeGadget;
	private Morph morph;
	private long lastGadgetUse;
	private long lastMorphUse;
	
	private Inventory lastInv;
	private Location lastLoc;
	
	private boolean muted;
	private long muteExpire;
	
	private boolean supportChatSpy = false;
	private boolean showDebug = false;
	private boolean premiumTested = false;
	private Birthday birthday;
	private boolean birthdayReset = false;
	
	private boolean loaded;
	
	public WickedUser (Player player) {
		this.uuid = player.getUniqueId();
		this.name = player.getName();
		this.firstLogin = player.getFirstPlayed();
	}
	
	// SETTER
	public void setPlayerMode(PlayerMode mode) { this.playerMode = mode; }
	public void setRank(Rank rank) { this.rank = rank; }
	public void setPrefix(String prefix) { this.prefix = prefix; }
	public void setNick(String nick) { this.nick = nick; }
	
	public void setCoins(int coins) { this.coins = coins; }
	public void setKills(int kills) { this.kills = kills; }
	public void setDeaths(int deaths) { this.deaths = deaths; }

	public void setLastLogin(long ticks) { this.lastLogin = ticks; }
	public void setLastLogout(long ticks) { this.lastLogout = ticks; }
	public Long setOnTime() { return this.onTime += (lastLogout - lastLogin); } // ontime berechnen
	public void setOnTime(long time) { this.onTime += time; }
	public void setLastLoc(Location loc) { this.lastLoc = loc; }
	public void setLastInv(Inventory inv) { this.lastInv = inv; }
	public void setPremiumTestStart(long premiumTestStart) { this.premiumTestStart = premiumTestStart; }
	
	public void setActiveGadget(Gadget activeGadget) { this.activeGadget = activeGadget; }
	public void setMorph(Morph morph) { this.morph = morph; }
	public void setLastGadgetUse(long lastGadgetUse) { this.lastGadgetUse = lastGadgetUse; }
	public void setLastMorphUse(long lastMorphUse) { this.lastMorphUse = lastMorphUse; }
	
	public void setMuted(boolean bool) { this.muted = bool; }
	public void setMuteExpire(long ticks) { this.muteExpire = ticks; }
	
	public void setSupportChatSpy(boolean bool) { this.supportChatSpy = bool; }
	public void setShowDebug(boolean bool) { this.showDebug = bool; }
	public void setPremiumTested(boolean premiumTested) { this.premiumTested = premiumTested; }
	public void setBirthday(Birthday birthday) { this.birthday = birthday; }
	public void setBirthdayReset(boolean bool) { this.birthdayReset = bool; }
	
	public void setLoaded(boolean loaded) { this.loaded = loaded; }
	
	// GETTER
	public UUID getUuid() { return this.uuid; }
	public String getUuidString() { return this.uuid.toString(); }
	public String getName() { return this.name; }
	public Player getPlayer() { return Bukkit.getPlayer(uuid); }
	public PlayerMode getPlayerMode() { return this.playerMode; }
	public Rank getRank() { return this.rank; }
	public boolean hasPermission(Rank rank) { return this.rank.permission() >= rank.permission(); }
	public String getPrefix() { return this.prefix; }
	public String getNick() { return this.nick; }
	public String getDisplayName() { if(nick.isEmpty()) { return name; } else { return hasPermission(Rank.PREMIUM) ? nick:name; } }
	public String getActivePrefix() { if(rank == Rank.PREMIUM && !prefix.isEmpty()) { return prefix; }
									else { return rank.prefix(); } }
	
	public int getCoins() { return this.coins; }
	public int getKills() { return this.kills; }
	public int getDeaths() { return this.deaths; }
	
	public Long getOnTime() { return this.onTime; } // ontime berechnen
	public long getFirstLogin() { return this.firstLogin; }
	public long getLastLogin() { return this.lastLogin; }
	public long getLastLogout() { return lastLogout; }
	public long getPremiumTestStart() { return premiumTestStart; }
	
	public Location getLastLoc() { return this.lastLoc; }
	public Inventory getLastInv() { return this.lastInv; }
	
	
	public Gadget getActiveGadget() { return activeGadget; }
	public Morph getMorph() { return morph; }
	
	public long getLastGadgetUse() { return lastGadgetUse; }
	public long getLastMorphUse() { return lastMorphUse; }
	
	public boolean isMuted() { return muted; }
	public long getMuteExpire() { return muteExpire; }
	
	public boolean isSupportChatSpy() { return supportChatSpy; }
	public boolean isShowDebug() { return showDebug; }
	public boolean hasPremiumTested() { return premiumTested; }
	public Birthday getBirthday() { return birthday; }
	public boolean isBirthdayReset() { return birthdayReset; }
	
	public boolean isLoaded() { return loaded; }

}
