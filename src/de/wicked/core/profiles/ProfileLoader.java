package de.wicked.core.profiles;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.mysql.MySQL;
import de.wicked.core.utils.Birthday;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.InventoryToBase64;

public class ProfileLoader extends BukkitRunnable{

	private WickedUser user;
	private MySQL mysql;
	
	private static final String INSERT = "INSERT INTO UserData(UUID,Name,firstLogin,lastLogin,lastLogout,lastX,lastY,lastZ) VALUES(?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE name=?";
	private static final String SELECT = "SELECT * FROM UserData WHERE uuid=?";

	public ProfileLoader(WickedUser user) {
		this.user = user;
		this.mysql = WickedCore.getMySQL();
	}
	
	@Override
	public void run() {
		
		Connection connection = null;
		
		try {
			connection = mysql.getCurrentConnection();
			
			PreparedStatement ps = connection.prepareStatement(INSERT);
			ps.setString(1, user.getUuidString());						// UUID
			ps.setString(2, user.getName());							// Name
			ps.setLong(3, System.currentTimeMillis());					// FirstLogin
			ps.setLong(4, System.currentTimeMillis());					// LastLogin
			ps.setLong(5, System.currentTimeMillis());					// LastLogout
			ps.setDouble(6, Bukkit.getWorld(Config.FREEBUILDWORLD.getString()).getSpawnLocation().getX());					// LastLoc (Freebuild Spawn)
			ps.setDouble(7, Bukkit.getWorld(Config.FREEBUILDWORLD.getString()).getSpawnLocation().getY());
			ps.setDouble(8, Bukkit.getWorld(Config.FREEBUILDWORLD.getString()).getSpawnLocation().getZ());
			ps.setString(9, user.getName());
			ps.execute();
			
			ps = connection.prepareStatement(SELECT);
			ps.setString(1, user.getUuidString());
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				user.setPlayerMode(PlayerMode.valueOf(rs.getString("PlayerMode")));
				user.setRank(Rank.valueOf(rs.getString("Rank")));
				user.setPrefix(rs.getString("Prefix"));
				user.setNick(rs.getString("Nick"));
				user.setActiveGadget(WickedCore.getGadgetManager().getGadget(rs.getString("activeGadget")));
				user.setLastGadgetUse(rs.getLong("lastGadgetUse"));
				user.setLastMorphUse(rs.getLong("lastMorphUse"));
				user.setLastLogin(rs.getLong("lastLogin"));
				user.setLastLogout(rs.getLong("lastLogout"));
				user.setOnTime(rs.getLong("OnTime"));
				user.setLastLoc(new Location(Bukkit.getWorld(Config.FREEBUILDWORLD.getString()),
								rs.getDouble("lastX"),
								rs.getDouble("lastY"),
								rs.getDouble("lastZ")));
				user.setMuted(rs.getBoolean("muted"));
				user.setMuteExpire(rs.getLong("muteExpire"));
				user.setSupportChatSpy(rs.getBoolean("supportChatSpy"));
				user.setShowDebug(rs.getBoolean("showDebug"));
				user.setKills(rs.getInt("Kills"));
				user.setDeaths(rs.getInt("Deaths"));
				user.setCoins(rs.getInt("Coins"));
				
				user.setLoaded(true);
			}
			
			// Geburtstag laden
			ps = connection.prepareStatement("SELECT * FROM UserBirthdays WHERE uuid=?");
			ps.setString(1, user.getUuidString());
			rs = ps.executeQuery();
			
			if(rs.next()) {
				user.setBirthday(Birthday.fromString(rs.getString("date")));
				user.setBirthdayReset(rs.getBoolean("reset"));
			}
			
			
			ps.close();
			rs.close();
			
			// Items laden

			Inventory inv = Bukkit.createInventory(user.getPlayer(), 54);
			YamlConfiguration config = WickedCore.getInvFile().getConfig();
			
			if(config.contains(user.getUuidString()+".freebuild")) {
				try {
					String data = config.getString(user.getUuidString()+".freebuild");
					inv = InventoryToBase64.fromBase64(data);
				} catch (IOException e) {
					e.printStackTrace();
					WickedCore.debug("Fehler beim Laden des Inventars (Player="+user.getPlayer()+")", false);
				}
			}
			
			user.setLastInv(inv);
			WickedCore.debug("Profil " + user.getName() + " geladen.", true);
			this.cancel();
			
		} catch(SQLException ex) {
			ex.printStackTrace();
			WickedCore.debug("Fehler beim Laden von Profil: " + user.getName(), true);
		} finally {
			if(connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					WickedCore.debug("Fehler beim Schlie�en der Connection", true);
				}
			}
		}
		
	}

}
