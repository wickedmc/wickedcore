package de.wicked.core.profiles;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.Utils;

public class OnTimeScheduler extends BukkitRunnable{

	private Player player;
	private int min;
	private double coins;
	
	public OnTimeScheduler(Player player, int min, double coins) {
		this.player = player;
		this.min = min;
		this.coins = coins;
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		if(!player.isOnline()) {
			this.cancel();
			return;
		}
		WickedCore.getEconomy().depositPlayer(player.getName(), coins);
		player.sendMessage(Utils.color("&6Wicked &8\u2503 &aDu hast f�r "+min+" Minuten Onlinezeit &e"+coins+" Geld&a bekommen!"));
	}
	
}
