package de.wicked.core.profiles;

import org.bukkit.GameMode;

public enum PlayerMode {
	LOBBY(GameMode.ADVENTURE),
	OTHER(GameMode.SURVIVAL),
	BUILD(GameMode.CREATIVE);
	
	private final GameMode gameMode;
	
	private PlayerMode(GameMode mode) {
		gameMode = mode;
	}
	
	public GameMode getGameMode() {
		return gameMode;
	}
}
