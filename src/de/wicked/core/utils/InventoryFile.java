package de.wicked.core.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.wicked.core.WickedCore;

public class InventoryFile {

	private static File f0f;
	private static YamlConfiguration cfg;
	
	static {
		f0f = new File(WickedCore.getInstance().getDataFolder(), "inventories.yml");
	}
	
	public boolean getBoolean(Player p, String path) {
		return cfg.getBoolean(p.getUniqueId() + path);
	}

	public double getDouble(Player p, String path) {
		return cfg.getDouble(p.getUniqueId() + path);
	}
	
	public double getInt(Player p, String path) {
		return cfg.getInt(p.getUniqueId() + path);
	}

	public String getString(Player p, String path) {
		return Utils.color(cfg.getString(p.getUniqueId() + path));
	}

	public List<String> getStringList(Player p, String path) {
		return cfg.getStringList(p.getUniqueId() + path);
	}

	public void load() {
		WickedCore.getInstance().getDataFolder().mkdirs();
		reload(false);
		try {
			cfg.save(f0f);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void save() {
		try {
			cfg.save(f0f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void set(Player p, String path, Object value, boolean save) {
		cfg.set(p.getUniqueId() + path, value);
		if (save) {
			try {
				cfg.save(f0f);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			reload(false);
		}
	}
	
	public void createSection(String section) {
		cfg.createSection(section);
	}
	
	public void saveItem(ConfigurationSection section, ItemStack item) {
		section.set("type", item.getType().name());
		section.set("amount", item.getAmount());
		section.set("durability", item.getDurability());
		section.set("name", item.getItemMeta().getDisplayName());
		section.set("lore", item.getItemMeta().getLore());
		int i = 0;
		for(Enchantment ench : item.getEnchantments().keySet()) {
			section.set("enchants." + i + ".name", ench.getName());
			section.set("enchants." + i + ".level", item.getEnchantments().get(ench));
		}
	}
	
	public ItemStack loadItem(ConfigurationSection section) {
		ItemStack itemStack = new ItemStack(Material.valueOf(section.getString("type")));
			itemStack.setAmount(section.getInt("amount"));
			itemStack.setDurability((short) section.getInt("durability"));
			ItemMeta meta = itemStack.getItemMeta();
			meta.setLore(section.getStringList("lore"));
			itemStack.setItemMeta(meta);
			
//			WickedCore.debug(section.getString("type"));
//			WickedCore.debug(section.getInt("durability"));

			if(cfg.contains(section.getCurrentPath()  + ".enchants")) {
//				WickedCore.debug("ja");
				ConfigurationSection cs = cfg.getConfigurationSection(section.getCurrentPath() + ".enchants");
				for(String s : cs.getKeys(false)) {
					Enchantment ench = Enchantment.getByName(cfg.getString(cs.getCurrentPath()+"." + s + ".name"));
					int level = cfg.getInt(cs.getCurrentPath()+"." + s + ".level");
					itemStack.addEnchantment(ench, level);
//					WickedCore.debug("s: " + s);
//					WickedCore.debug("ench: " + ench.getName() + ", level: " + level);
				}
			}
//			WickedCore.debug("nein");
//			WickedCore.debug(itemStack.getEnchantments());
		
		return itemStack;
	}

	public void reload(boolean complete) {
		if (complete) {
			load();
		} else {
			cfg = YamlConfiguration.loadConfiguration(f0f);
		}
	}
	
	public YamlConfiguration getConfig() {
		return cfg;
	}
}
