package de.wicked.core.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;

public class Storage {

	private static boolean globalmute;
	private static Set<Player> mutedPlayers = new HashSet<>();
	
	
	public static boolean isGlobalmuteActive() {
		return globalmute;
	}
	
	public static boolean isMuted(Player p) {
		if(mutedPlayers.contains(p))
			return true;
		else
			return false;
	}
	
	public Collection<Player> getMutedPlayers() {
		return mutedPlayers;
	}
	
	public static void mute(Player p) {
		mutedPlayers.add(p);
	}
	
	public static void unmute(Player p) {
		mutedPlayers.remove(p);
	}
}
