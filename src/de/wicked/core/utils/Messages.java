package de.wicked.core.utils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.wicked.core.WickedCore;

public enum Messages {

	NOPERM("messages.noperm", "&cDu darfst das nicht!"),
	NOPLAYER("messages.noplayer", "Dieser Befehl ist nur f�r Spieler."),
	PLRNOTFOUND("messages.playernotfound", "&cDieser Spieler ist nicht online."),
	UNKNOWNCMD("messages.unknowncmd", "Unknown command. Type \"/help\" for help."),
	DEBUG("settings.debugformat", "[DEBUG] %s"),
	WARTUNG_KICK("messages.wartung.kick", "&cDer Server befindet sich derzeit in Wartungen!"),
	FAILJOIN("", "&b[Join] &e%s versuchte zu joinen: &c%s"),
	
	TC_TOGGLE_ON("commands.teamchat.toggle.on", ""),
	TC_TOGGLE_OFF("commands.teamchat.toggle.off", ""),
	
	JOINMSG("events.join", "&8[&a+&8] %c%d"),
	QUITMSG("events.quit", "&8[&c-&8] %c%d"),
	
	PREFIX_SYNTAX("commands.prefix.help.syntax", "%s Nutze &6/prefix <prefix> &7(Nur ein Wort!)"),
	PREFIX_HELP_LIST("commands.prefix.help.list", "%s &6\u25B6 &e/prefix list &6- Liste Prefixw�nsche auf"),
	PREFIX_HELP_DONE("commands.prefix.help.done", "%s &6\u25B6 &e/prefix done <#> &6- Markiere einen Eintrag als erledigt"),
	PREFIX_HELP_CLOSE("commands.prefix.help.close", "%s &6\u25B6 &e/prefix close <#> &6- Schlie�e einen Eintrag"),;
    
    private static YamlConfiguration cfg;
    private static final File f1f;
    private static final File localeFolder;
    private final String path;
    private final String value;

    static {
        localeFolder = new File("plugins/WickedCore" + File.separator + "locales");
        f1f = new File(localeFolder, Config.LOCALE.getString() + ".yml");
    }

    private Messages(String path, String val) {
        this.path = path;
        this.value = val;
    }
	
	public void send(Player p, String... args) {
		p.sendMessage(Utils.color(Config.PREFIX.getString() + String.format(getString(), (Object[])args)));
	}
	
	public void send(Player p) {
		p.sendMessage(Utils.color(Config.PREFIX.getString() + getString()));
	}

    public String getPath() {
        return this.path;
    }

    public String getDefaultValue() {
        return this.value;
    }

    public String getString() {
        return cfg.getString(this.path).replaceAll("&((?i)[0-9a-fk-or])", "\u00a7$1");
    }
    
    public String getString(String... args) {
        return String.format(getString(), (Object[])args);
    }

    public void send(CommandSender s) {
        s.sendMessage(getString());
    }

    public void send(CommandSender s, Map<String, String> map) {
        String msg = getString();
        for (String string : map.keySet()) {
            msg = msg.replaceAll(string, (String) map.get(string));
        }
        s.sendMessage(msg);
    }

    public static void load() throws IOException {
        localeFolder.mkdirs();
        if (f1f.exists()) {
            reload(false);
            for (Messages c : values()) {
                if (!cfg.contains(c.getPath())) {
                    c.set(c.getDefaultValue(), false);
                }
            }
            try {
                cfg.save(f1f);
                return;
            } catch (IOException ex) {
                ex.printStackTrace();
                return;
            }
        }
        try {
        	WickedCore.getInstance().saveResource("locales" + File.separator + Config.LOCALE.getString() + ".yml", true);
            File locale = new File(WickedCore.getInstance().getDataFolder(), Config.LOCALE.getString() + ".yml");
            if (locale.exists()) {
                FileUtils.moveFile(locale, f1f);
            }
            reload(false);
        } catch (IllegalArgumentException e) {
            reload(false);
            for (Messages c2 : values()) {
                if (!cfg.contains(c2.getPath())) {
                    c2.set(c2.getDefaultValue(), false);
                }
            }
            try {
                cfg.save(f1f);
            } catch (IOException ioex) {
                ioex.printStackTrace();
            }
        }
    }

    public void set(Object value, boolean save) throws IOException {
        cfg.set(this.path, value);
        if (save) {
            try {
                cfg.save(f1f);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            reload(false);
        }
    }

    public static void reload(boolean complete) throws IOException {
        if (complete) {
            load();
        } else {
            cfg = YamlConfiguration.loadConfiguration(f1f);
        }
    }

    public static Messages fromPath(String path) {
        for (Messages loc : values()) {
            if (loc.getPath().equalsIgnoreCase(path)) {
                return loc;
            }
        }
        return null;
    }
	
}
