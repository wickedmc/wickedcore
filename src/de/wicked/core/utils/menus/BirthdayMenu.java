package de.wicked.core.utils.menus;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.Birthday;
import de.wicked.core.utils.ItemStackBuilder;
import de.wicked.core.utils.Utils;



public class BirthdayMenu extends Menu {

	public BirthdayMenu(String title, int size) {
		super(title, size);
		
		getInventory().setItem(0, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Tag: &e0")
				.withLore("&7Klicken um zu erh�hen").withAmount(0).withDurability(4).build());
		getInventory().setItem(1, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Tag: &e1")
				.withLore("&7Klicken um zu erh�hen").withAmount(1).withDurability(4).build());
		
		getInventory().setItem(2, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Monat: &60")
				.withLore("&7Klicken um zu erh�hen").withAmount(0).withDurability(1).build());
		getInventory().setItem(3, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Monat: &61")
				.withLore("&7Klicken um zu erh�hen").withAmount(1).withDurability(1).build());
		
		getInventory().setItem(4, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Jahr: &c2")
				.withLore("&7Klicken um zu erh�hen").withAmount(2).withDurability(14).build());
		getInventory().setItem(5, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Jahr: &c0")
				.withLore("&7Klicken um zu erh�hen").withAmount(0).withDurability(14).build());
		getInventory().setItem(6, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Jahr: &c0")
				.withLore("&7Klicken um zu erh�hen").withAmount(0).withDurability(14).build());
		getInventory().setItem(7, new ItemStackBuilder(Material.STAINED_GLASS).withName("&7Jahr: &c0")
				.withLore("&7Klicken um zu erh�hen").withAmount(0).withDurability(14).build());
		
		getInventory().setItem(8, new ItemStackBuilder(Material.EMERALD_BLOCK).withName("&aBest�tigen").build());
	}

	@Override
	public void click(Player player, ItemStack item) {
		try {
			
			String itemName = getFriendlyName(item);
			
			if(item.getType() == Material.STAINED_GLASS) {
				if(itemName.contains("Tag")) {
					addOne(item, "&7Tag: &e");
				} else if(itemName.contains("Monat")) {
					addOne(item, "&7Monat: &6");
				} else if(itemName.contains("Jahr")) {
					addOne(item, "&7Jahr: &c");
				}
			} else if(item.getType() == Material.EMERALD_BLOCK) {
				if(itemName.equalsIgnoreCase("Best�tigen")) {
					String day = getInventory().getItem(0)+""+getInventory().getItem(1);
					String month = getInventory().getItem(2)+""+getInventory().getItem(3);
					String year = getInventory().getItem(4)+""+getInventory().getItem(5)+""+getInventory().getItem(6)+""+getInventory().getItem(7);
				
					Birthday bday = new Birthday(Integer.parseInt(day), Integer.parseInt(month), Integer.parseInt(year));
					WickedCore.getUser(player).setBirthday(bday);
					
					WickedCore.debug(player.getName()+" hat seinen Bday eingetragen: "+bday.toString(), true);
					player.sendMessage(Utils.color("&6[Geburtstag] &aDein Geburtstag wurde erfolgreich eingetragen! &6"+bday.toString()));
				}
			}
			
		} catch(Exception ex) {}
	}

	private void addOne(ItemStack item, String name) {
		int amount = item.getAmount();
		item.setAmount(amount < 9 ? amount+1:0);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name+item.getAmount());
		item.setItemMeta(meta);
	}
	
}
