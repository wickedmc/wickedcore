package de.wicked.core.utils.menus;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.ItemStackBuilder;

@SuppressWarnings("unused")
public class BanMenu extends Menu{

	public BanMenu(String title, int size) {
		super(title, size);
		
	}

	@Override
	public void click(Player player, ItemStack itemStack) {
		
	}

	private int calcSize() {
		List<String> reasons = Config.BANREASONS.getStringList();
		/*reasons = 5
		 * i = 27
		 * 27 < 32
		 * 
		 * i=36
		 * 36 > 32 -> break
		 */
		int i = 27;
		while(i < 27+reasons.size()) {
			i+=9;
		}
		return i;
	}
	
	@SuppressWarnings("deprecation")
	private ItemStack[] getReasons() {
		List<String> reasonList = Config.BANREASONS.getStringList();
		List<ItemStack> reasons = new ArrayList<>();
		for(String reason : reasonList) {
			String name, itemS, minS, maxS; // Name, itemString, minimumString, maximumString
			
			name = reason.split(" ")[0].replaceAll("_", " ");
			itemS = reason.split(" ")[1];
			minS = reason.split(" ")[2];
			maxS = reason.split(" ")[3];
			
			String id = "1", data = "0";
			if(itemS.contains(":")) {
				id = itemS.split(":")[0];
				data = itemS.split(":")[1];
				try {
					Integer.parseInt(id);
					Integer.parseInt(data);
				} catch(Exception ex) { WickedCore.debug("BanMenu Item ist keine Integer!", false);
										id = "1"; data = "0";}
			} else {
				try {
					id = itemS;
					Integer.parseInt(id);
				} catch(Exception ex) { WickedCore.debug("BanMenu Item ist keine Integer!", false);
										id = "1"; }
			}
			ItemStack item = new ItemStackBuilder(new ItemStack(Integer.parseInt(id)))
							.withDurability(Integer.parseInt(data))
							.withName(name)
							.withLore("&7&oKleines Vergehen: "+minS)
							.withLore("&7&oGro�es Vergehen: "+maxS).build();
			reasons.add(item);
		}
		ItemStack[] reasonsArray = new ItemStack[reasons.size()];
		reasonsArray = reasons.toArray(reasonsArray);
		return reasonsArray;
	}
	
}
