package de.wicked.core.utils.menus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import de.wicked.core.WickedCore;
import de.wicked.core.mysql.MySQL;
import de.wicked.core.utils.ItemStackBuilder;

public class NavigationMenu extends Menu{

	private static HashMap<String, Location> locations = new HashMap<>();
	
	public NavigationMenu(String title) {
		super(title, 54);
		
		// Locations speichern
		String[] names = {"spawn", "mitte", "freebuild", "pvp", "serverteam", "creative", "minigames"};
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				if(i < names.length) {
					locations.put(names[i], loadLoc(names[i]));
					WickedCore.debug("NavMenu Location " + names[i] + " geladen", false);
					i++;
				} else {
					this.cancel();
				}
			}
		}.runTaskTimer(WickedCore.getInstance(), 0, 2);
		
		ItemStack hellBlau = new ItemStackBuilder(Material.STAINED_GLASS_PANE).withName("&f").withDurability(3).build();
		
		ItemStack spawn = new ItemStackBuilder(Material.NETHER_STAR).withName("&eSpawn").build();
		ItemStack mitte = new ItemStackBuilder(Material.COMPASS).withName("&cNavigation").build();
		ItemStack team = new ItemStackBuilder(Material.ARMOR_STAND).withName("&aServerteam").build();
		ItemStack plot = new ItemStackBuilder(Material.STONE_SLAB2).withName("&bCreative").withLore("").withLore("&c&lCOMING SOON").build();
		ItemStack freebuild = new ItemStackBuilder(Material.WOOD_PICKAXE).withName("&2Freebuild").build();
		ItemStack pvp = new ItemStackBuilder(Material.IRON_SWORD).withName("&7PvP").build();
		ItemStack minigames = new ItemStackBuilder(Material.BED).withName("&4Minigames").build();
		
		for(int i = 0; i < 54; i++) {
			getInventory().setItem(i, hellBlau);
		}
		
		getInventory().setItem(4, freebuild);
		getInventory().setItem(11, plot);
		getInventory().setItem(15, pvp);
		getInventory().setItem(31, mitte);
		getInventory().setItem(28, team);
		getInventory().setItem(34, minigames);
		getInventory().setItem(49, spawn);
		
	}

	@Override
	public void click(Player player, ItemStack itemStack) {
		
		try {
			
			String itemName = getFriendlyName(itemStack);
			
			switch(itemName) {
			case "Spawn": //zum spawn tpn
				player.teleport(getLoc("spawn"));
				break;
			case "Navigation": //in die mitte tp'n
				player.teleport(getLoc("mitte"));
				break;
			case "Freebuild": //in die freebuildwelt tpn
				player.teleport(getLoc("freebuild"));
				break;
			case "PvP": //in die pvp welt tpn
				player.teleport(getLoc("pvp"));
				break;
			case "Serverteam": //zum team tpn
				player.teleport(getLoc("serverteam"));
				break;
			case "Creative": //zur crea insel tpn
				player.teleport(getLoc("creative"));
				break;
			case "Minigames": //auf minigames server
				player.teleport(getLoc("minigames"));
				break;
				default:
					return;
			}
		} catch(Exception ex) {
			return;
		}
		
	}
	
	public static Location getLoc(String name) {
		return locations.get(name);
	}

	private Location loadLoc(String name) {
		
		MySQL mysql = WickedCore.getMySQL();
		Location loc = null;
		
		ResultSet rs = mysql.getResult("SELECT * FROM SpawnInfo WHERE name='"+name+"'");
		
		try {
			if(rs.next()) {
				loc = new Location(Bukkit.getWorld(rs.getString("world")),
						rs.getDouble("x"),
						rs.getDouble("y"),
						rs.getDouble("z"),
						rs.getFloat("yaw"),
						rs.getFloat("pitch"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			loc = null;
		}
		
		return loc;
		
	}
	

}
