package de.wicked.core.utils;

public class Birthday {
	
	private int day;
	private int month;
	private int year;
	
	public Birthday(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public int getDay() { return day; }
	public int getMonth() { return month; }
	public int getYear() { return year; }
	
	public void setDay(int d) { day = d; }
	public void setMonth(int m) { month = m; }
	public void setYear(int y) { year = y; }
	
	public String toString() {
		// 15-09-1999
		return this == null ? (day < 10 ? "0"+day:day)+"-"+(month < 10 ? "0"+month:month)+"-"+year:"";
	}
	
	public static Birthday fromString(String input) {
		if(input.equals("")) { return null; }
		try {
			String[] s = input.split("-");
			return new Birthday(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2]));
		} catch(Exception ex) {
			return null;
		}
	}
	
}
