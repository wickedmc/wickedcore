package de.wicked.core.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffectType;

import de.wicked.core.WickedCore;
import de.wicked.core.profiles.PlayerMode;
import de.wicked.core.profiles.Rank;
import de.wicked.core.profiles.WickedUser;

public class Utils {

	public static String color(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	public static void heal(Player p) {
		p.setHealth(20.0);
		p.setFoodLevel(20);
	}
	
	public static String stripColor(String s) {
		return ChatColor.stripColor(s);
	}
	
	public static int minutesToTicks(int minutes) {
		return minutes * secondsToTicks(60);
	}
	
	public static int secondsToTicks(int seconds) {
		return seconds*20;
	}
	
	public static int parseInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch(Exception ex) {
			return -1;
		}
	}
	
	public static void broadcastAll(String msg) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(color(msg));
		}
	}
	
	public static void broadcastTeam(String msg) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			WickedUser user = WickedCore.getUser(p);
			if(user.hasPermission(Rank.BUILDER)) {
				p.sendMessage(color(msg));
			}
		}
	}
	
	public static void broadcastAdmins(String msg) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			WickedUser user = WickedCore.getUser(p);
			if(user.hasPermission(Rank.ADMIN)) {
				p.sendMessage(color(msg));
			}
		}
	}
	
	public static String milliesToString(long start, long end) {
		long current = start;
		long difference = end-current; //millisekunden
		int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
		
		while(difference > 1000) { difference -= 1000; secs++; }
		while(secs > 60) { secs -= 60; mins++; }
		while(mins > 60) { mins -= 60; hours++; }
		while(hours > 24) { hours -= 24; days++; }
		while(days > 7) { days -= 7; weeks++; }
		while(weeks > 52) { weeks -= 52; years++; }
		
		return ((years > 0) ? ((years > 1) ? years+" Jahre, ":years+" Jahr, "):"")
				+ ((weeks > 0) ? ((weeks > 1) ? weeks+" Wochen, ":weeks+" Woche, "):"")
				+ ((days > 0) ? ((days > 1) ? days+" Tage, ":days+" Tag, "):"")
				+ ((hours > 0) ? ((hours > 1) ? hours+" Stunden, ":hours+" Stunde, "):"")
				+ ((mins > 0) ? ((mins > 1) ? mins+" Minuten, ":mins+" Minute, "):"")
				+ ((secs > 0) ? ((secs > 1) ? secs+" Sekunden":secs+" Sekunde"):"");
	}
	
	public static String milliesToString(long millies) {
		int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
		
		while(millies > 1000) { millies -= 1000; secs++; }
		while(secs > 60) { secs -= 60; mins++; }
		while(mins > 60) { mins -= 60; hours++; }
		while(hours > 24) { hours -= 24; days++; }
		while(days > 7) { days -= 7; weeks++; }
		while(weeks > 52) { weeks -= 52; years++; }
		
		return ((years > 0) ? ((years > 1) ? years+" Jahre, ":years+" Jahr, "):"")
				+ ((weeks > 0) ? ((weeks > 1) ? weeks+" Wochen, ":weeks+" Woche, "):"")
				+ ((days > 0) ? ((days > 1) ? days+" Tage, ":days+" Tag, "):"")
				+ ((hours > 0) ? ((hours > 1) ? hours+" Stunden, ":hours+" Stunde, "):"")
				+ ((mins > 0) ? ((mins > 1) ? mins+" Minuten, ":mins+" Minute, "):"")
				+ ((secs > 0) ? ((secs > 1) ? secs+" Sekunden":secs+" Sekunde"):"");
	}
	
	public static String milliesToDate(long millies) {
		
		if(millies == -1) {
			return "f�r immer";
		}
		
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(millies);
		
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);
		int mHour = calendar.get(Calendar.HOUR_OF_DAY);
		int mMin = calendar.get(Calendar.MINUTE);
		int mSec = calendar.get(Calendar.SECOND);
		
		return mDay + "." + (mMonth+1) + "." + mYear + ", " + mHour + ":" + mMin + ":" + mSec + " Uhr";
	}
	
	public static long stringToMillies(String s) {
		long seconds;
		char[] zeitInput = s.toCharArray();
		String zeitChache = "";
		int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
		for(char c : zeitInput) {
			if(isNumber(c)) {
				zeitChache += c;
				continue;
			} else {
				switch(c) {
				case 'y': years = Integer.parseInt(zeitChache); zeitChache = ""; continue;
				case 'w': weeks = Integer.parseInt(zeitChache); zeitChache = ""; continue;
				case 'd': days = Integer.parseInt(zeitChache); zeitChache = ""; continue;
				case 'h': hours = Integer.parseInt(zeitChache); zeitChache = ""; continue;
				case 'm': mins = Integer.parseInt(zeitChache); zeitChache = ""; continue;
				case 's': secs = Integer.parseInt(zeitChache); zeitChache = ""; continue;
					default: return 0;
				}
			}
		}
		seconds = secs+(mins*60)+(hours*3600)+(days*24*3600)+(weeks*7*24*3600)+(years*52*7*24*3600);
		return seconds;
	}
	
	public static boolean isNumber(char ch) {
		char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};
		for(char c : numbers) {
			if(c == ch) {
				return true;
			}
		}
		return false;
	}
	
	public static void setLobby(Player player) {
		WickedUser user = WickedCore.getUser(player);

		if(user.getPlayerMode() == PlayerMode.LOBBY) {
			System.out.println("User wird Lobby-gesetzt");
			player.setGameMode(GameMode.ADVENTURE);
			player.getInventory().clear();
			player.getInventory().setArmorContents(null);
			player.getInventory().setItem(0, new ItemStackBuilder(Material.COMPASS).withName("&bNavigator").build());
			player.getInventory().setItem(4, new ItemStackBuilder(Material.CHEST).withName("&eKosmetik Menu").build());
			player.getInventory().setItem(5, user.getActiveGadget().getItem());   
			player.removePotionEffect(PotionEffectType.INVISIBILITY);
			player.setFireTicks(0);
			Utils.heal(player);

			if(user.hasPermission(Rank.PREMIUM)) {
				player.setAllowFlight(true);
				player.setFlying(true);
			}
		}
		
	}
	
	public static Color getColorByInt(int i) {
		switch(i) {
		case 0: return Color.AQUA;
		case 1: return Color.BLACK;
		case 2: return Color.BLUE;
		case 3: return Color.FUCHSIA;
		case 4: return Color.GRAY;
		case 5: return Color.GREEN;
		case 6: return Color.LIME;
		case 7: return Color.MAROON;
		case 8: return Color.NAVY;
		case 9: return Color.OLIVE;
		case 10: return Color.ORANGE;
		case 11: return Color.PURPLE;
		case 12: return Color.RED;
		case 13: return Color.SILVER;
		case 15: return Color.TEAL;
		case 16: return Color.WHITE;
		case 17: return Color.YELLOW;
		}
		return Color.WHITE;
	}
	
	public static Location randomRadiusLoc(Location l, int radius) {
        int max = 5;
        double x = Math.random() * (max * 2) - max;
        double z = Math.random() * (max * 2) - max;
        return l.add(x,0,z);
	}
	
	public static FireworkMeta randomFireworkMeta(Firework fw) {
		// Credit: Rprrr (Bukkit.org)
		FireworkMeta fm = fw.getFireworkMeta();
		
		//Random Generator
		Random r = new Random();
		
		//Get the type
		int rt = r.nextInt(5)+1;
		Type type = Type.BALL;
		if(rt==1) type = Type.BALL;
		if(rt==2) type = Type.BALL_LARGE;
		if(rt==3) type = Type.BURST;
		if(rt==4) type = Type.CREEPER;
		if(rt==5) type = Type.STAR;
		
		//Random colors
		int r1i = r.nextInt(18);
		int r2i = r.nextInt(18);
		Color c1 = getColorByInt(r1i);
		Color c2 = getColorByInt(r2i);
		
		//Create Effect
		FireworkEffect effect = FireworkEffect.builder()
											.flicker(r.nextBoolean())
											.withColor(c1).withFade(c2)
											.with(type)
											.trail(r.nextBoolean()).build();
		
		//Applying the effect to the meta
		fm.addEffect(effect);
		
		//Random power
		int rp = r.nextInt(2)+1;
		fm.setPower(rp);
		
		return fm;
	}
	
	public static void setInventory(Player p, Inventory inv) {
		p.getInventory().setContents(inv.getContents());
	}
}
