package de.wicked.core.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public enum Config {

	DEBUG("debug", Boolean.valueOf(true), "debug mode"),
	STATUS("status", "ONLINE", "ONLINE oder WARTUNG"),
	DB_HOST("database.host", "localhost", "db connection"),
	DB_PORT("database.port", "3306", "db connection"),
	DB_DATABASE("database.database", "wicked", "db connection"),
	DB_USER("database.username", "root", "db connection"),
	DB_PASS("database.password", "", "db connection"),
	TEAM_LISTE("teamliste", Arrays.asList("Didi_Skywalker","DanCr4fter","dbleoo"
			,"Definim","Alezcoedar","Herr_Cules","Nymus","13_m","Fladek7575"), "spieler die joinen d�rfen"),
	PREFIX("messages.prefix", "&2WKD &8\u00BB ", "Prefix vor den Nachrichten"),
	LOBBYWORLD("settings.lobbyworld", "voidworld", "Lobbywelt"),
	FREEBUILDWORLD("settings.freebuildWorld", "farm", "Freebuildworld"),
	CHATFORMAT("settings.chat.format", "%p &8\u2503 %c%d &8\u00BB &r%m", ""),
	SUPPCHATFORMAT("settings.chat.suppchat", "&6&l[&e&lSC&6&l] &e%player &8\u00BB &a%m", "SupportChat Format"),
	TC_FORMAT("settings.teamchat.format", "&c[TeamChat] %s&c: &a%s", "Teamchat Format"),
	PREFIX_BLACKLIST("settings.prefix.blacklist", Arrays.asList("", ""), "Blacklist"),
	STARTUP("messages.startup", "\n\nWicked%s\nVersion %s\nENABLED\n", "Startup Message"),
	NICK_BLACKLIST("settings.nick.blacklist", Arrays.asList("", ""), "Blacklist"),
	BROADCAST_FAILJOIN("settings.broadcast.failJoins", Boolean.valueOf(false), "broadcast fail joins"),
	FIRST_LOGIN_COINS("settings.firstlogincoins", 100, "coins to get when you first login"),
	ONTIMEMONEY("ontimemoney.enabled", Boolean.valueOf(true), ""),
	ONTIMEMONEY_TIME("ontimemoney.time", 2, ""),
	ONTIMEMONEY_MONEY("ontimemoney.money", 20, ""),
	BANREASONS("banreasons", Arrays.asList("Griefing 2 3d 3w", "Hacking 267 1d 1w"), "Reason item minTime maxTime"),
	LOCALE("messages.locale", "messages", "Definiere die Messagesdatei"),
	SYNCTIME("settings.worlds.lobby.synctime", Boolean.valueOf(true), ""),
	PROXYIP("settings.proxyip", "128.0.125.97", "");

	private static YamlConfiguration cfg;
	private static File f0f;
	private final String path;
	private final String description;
	private final Object value;

	static {
		f0f = new File("plugins/WickedCore/", "config.yml");
	}

	private Config(String path, Object value, String description) {
		this.path = path;
		this.value = value;
		this.description = description;
	}
	
	public void send(Player p, String... args) {
		p.sendMessage(Utils.color(Config.PREFIX.getString() + String.format(getString(), (Object[])args)));
	}
	
	public void send(Player p) {
		p.sendMessage(Utils.color(Config.PREFIX.getString() + getString()));
	}

	public String getPath() {
		return this.path;
	}

	public String getDescription() {
		return this.description;
	}

	public Object getDefaultValue() {
		return this.value;
	}

	public boolean getBoolean() {
		return cfg.getBoolean(this.path);
	}

	public double getDouble() {
		return cfg.getDouble(this.path);
	}
	
	public int getInt() {
		return cfg.getInt(this.path);
	}

	public String getString() {
		return Utils.color(cfg.getString(this.path));
	}

	public String getString(String... args) {
        return String.format(getString(), (Object[])args);
    }
	
	public List<String> getStringList() {
		return cfg.getStringList(this.path);
	}

	public static void load() {
		//WickedCore.getInstance().getDataFolder().mkdirs();
		reload(false);
		String header = "";
		for (Config c : values()) {
			header = header + c.getPath() + ": " + c.getDescription()
					+ System.lineSeparator();
			if (!cfg.contains(c.getPath())) {
				c.set(c.getDefaultValue(), false);
			}
		}
		cfg.options().header(header);
		try {
			cfg.save(f0f);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void set(Object value, boolean save) {
		cfg.set(this.path, value);
		if (save) {
			try {
				cfg.save(f0f);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
			reload(false);
		}
	}

	public static void reload(boolean complete) {
		if (complete) {
			load();
		} else {
			cfg = YamlConfiguration.loadConfiguration(f0f);
		}
	}

}
