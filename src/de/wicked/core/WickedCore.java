package de.wicked.core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import de.slikey.effectlib.EffectManager;
import de.wicked.core.bans.BanManager;
import de.wicked.core.bans.CommandBan;
import de.wicked.core.bans.CommandBaninfo;
import de.wicked.core.bans.CommandBanlist;
import de.wicked.core.bans.CommandTempBan;
import de.wicked.core.bans.CommandUnban;
import de.wicked.core.chat.AdminAlert;
import de.wicked.core.chat.GeneralChatListener;
import de.wicked.core.commands.CommandGeburtstag;
import de.wicked.core.commands.CommandKick;
import de.wicked.core.commands.CommandMode;
import de.wicked.core.commands.CommandNick;
import de.wicked.core.commands.CommandOpensaveinv;
import de.wicked.core.commands.CommandPrefix;
import de.wicked.core.commands.CommandRank;
import de.wicked.core.commands.CommandSay;
import de.wicked.core.commands.CommandUhr;
import de.wicked.core.commands.CommandUser;
import de.wicked.core.commands.CommandUsers;
import de.wicked.core.commands.CommandWicked;
import de.wicked.core.commands.HelpChat;
import de.wicked.core.commands.PremiumCommand;
import de.wicked.core.commands.SpawnCommands;
import de.wicked.core.cosmetics.CosmeticMenu;
import de.wicked.core.cosmetics.gadgets.FiretrailGadget;
import de.wicked.core.cosmetics.gadgets.FireworksGadget;
import de.wicked.core.cosmetics.gadgets.GadgetManager;
import de.wicked.core.cosmetics.gadgets.GadgetMenu;
import de.wicked.core.cosmetics.gadgets.HellfireGadget;
import de.wicked.core.cosmetics.gadgets.LavatrailGadget;
import de.wicked.core.cosmetics.gadgets.RainbowGadget;
import de.wicked.core.cosmetics.gadgets.RocketGadget;
import de.wicked.core.cosmetics.morphs.MorphMenu;
import de.wicked.core.listeners.EntityListener;
import de.wicked.core.listeners.PlayerJoinListener;
import de.wicked.core.listeners.PlayerListener;
import de.wicked.core.listeners.PlayerLoginListener;
import de.wicked.core.listeners.PlayerQuitListener;
import de.wicked.core.listeners.ServerListener;
import de.wicked.core.mysql.MySQL;
import de.wicked.core.profiles.ProfileLoader;
import de.wicked.core.profiles.ProfileSaver;
import de.wicked.core.profiles.WickedUser;
import de.wicked.core.utils.Config;
import de.wicked.core.utils.InventoryFile;
import de.wicked.core.utils.Messages;
import de.wicked.core.utils.Utils;
import de.wicked.core.utils.menus.BirthdayMenu;
import de.wicked.core.utils.menus.MenuManager;
import de.wicked.core.utils.menus.NavigationMenu;

public class WickedCore extends JavaPlugin{

	private static WickedCore INSTANCE;
	private static InventoryFile invFile;
	private static File logFile;
	private static MySQL mysql = new MySQL();
	private static ServerStatus serverStatus;
	private static Map<UUID, WickedUser> users = new HashMap<>();
	private static Map<Player, BukkitRunnable> onTimeTasks = new HashMap<>();
	private static BanManager banManager;
	private static MenuManager menuManager;
	private static GadgetManager gadgetManager;
	private static EffectManager effectManager;
	private static Permission perms;
	private static Economy eco;
	private static GameScheduler gameScheduler;
	private static GregorianCalendar calendar = new GregorianCalendar();
	
	@Override
	public void onEnable() {
		if(!checkRequirements()) {
			System.out.println("Es wurden nicht alle Vorraussetzungen gefunden!");
			System.out.println("WickedCore braucht: EffectLib");
			getServer().getPluginManager().disablePlugin(this);
		}
		INSTANCE = this;
		loadLogFile();
		banManager = new BanManager();
		menuManager = new MenuManager(this);
		gadgetManager = new GadgetManager(this);
		effectManager = new EffectManager(this);
		invFile = new InventoryFile();

		setupPermissions();
		setupEconomy();
		registerCommands();
		loadFiles();
		loadMySQL();
		loadUsers();
		registerGadgets();
		registerMenus();
		registerListeners();
		serverStatus = ServerStatus.valueOf(Config.STATUS.getString());
		
		gameScheduler = new GameScheduler();
		gameScheduler.runTaskTimerAsynchronously(this, 10, 20);
		
		System.out.println(Config.STARTUP.getString("Core", getDescription().getVersion()));
	}
	
	@Override
	public void onDisable() {
		
		for(WickedUser u : users.values()) {
			if(u.getActiveGadget().isRunning(u.getPlayer())) u.getActiveGadget().cancel(u.getPlayer());
			if(u.getMorph() != null) u.getMorph().cancel();
		}
		
		mysql.closeConnection();
		INSTANCE = null;
		effectManager.dispose();
	}
	
	private void registerCommands() {
		getCommand("mode").setExecutor(new CommandMode());// behalten, f�r admins
		getCommand("ban").setExecutor(new CommandBan());// bungee
		getCommand("unban").setExecutor(new CommandUnban());//bungee
		getCommand("tempban").setExecutor(new CommandTempBan());//bungee
		getCommand("banlist").setExecutor(new CommandBanlist());//bungee
		getCommand("baninfo").setExecutor(new CommandBaninfo());//bungee
		getCommand("prefix").setExecutor(new CommandPrefix());//behalten
		getCommand("nick").setExecutor(new CommandNick());//behalten
		getCommand("opensaveinv").setExecutor(new CommandOpensaveinv());//behalten
		getCommand("user").setExecutor(new CommandUser());//behalten
		getCommand("uhr").setExecutor(new CommandUhr());//behalten
		getCommand("heartreset").setExecutor(new CommandUhr());//raus
		getCommand("wicked").setExecutor(new CommandWicked());
		getCommand("rank").setExecutor(new CommandRank());
		getCommand("say").setExecutor(new CommandSay());
		getCommand("users").setExecutor(new CommandUsers());
		getCommand("spawn").setExecutor(new SpawnCommands());
		getCommand("lobby").setExecutor(new SpawnCommands());
		getCommand("kick").setExecutor(new CommandKick());
		getCommand("support").setExecutor(new HelpChat());
		getCommand("setpremium").setExecutor(new PremiumCommand());
		getCommand("birthday").setExecutor(new CommandGeburtstag());
	}
	
	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new AdminAlert(), this);
		pm.registerEvents(new PlayerListener(), this);
		pm.registerEvents(new GeneralChatListener(), this);
		pm.registerEvents(new EntityListener(), this);
		pm.registerEvents(new PlayerJoinListener(), this);
		pm.registerEvents(new PlayerLoginListener(), this);
		pm.registerEvents(new PlayerQuitListener(), this);
		pm.registerEvents(new ServerListener(), this);
		pm.registerEvents(new HelpChat(), this);
	}
	
	private void registerMenus() {
		menuManager.addMenu("birthday", new BirthdayMenu("&6Geburtstags-Menu", 9));;
		menuManager.addMenu("cosmetic", new CosmeticMenu(Utils.color("&6<>=-- &eKostemik Menu &6--=<>")));
		menuManager.addMenu("nav", new NavigationMenu(Utils.color("&9<>=-- &bNavigation &9--=<>")));
		menuManager.addMenu("gadgets", new GadgetMenu(Utils.color("&2<>=-- &aGadgets &2--=<>")));
		menuManager.addMenu("morphs", new MorphMenu(Utils.color("&a<>=-- &2Morphs &a--=<>")));
	}
	
	private boolean checkRequirements() {
		Plugin plugin = getServer().getPluginManager().getPlugin("EffectLib");
		if(plugin ==  null) { return false; }
		plugin = getServer().getPluginManager().getPlugin("Vault");
		if(plugin ==  null) { return false; }
		return true;
	}
	
	private void registerGadgets() {
		gadgetManager.addGadget("firetrail", new FiretrailGadget(40, "Firetrail"));	
		gadgetManager.addGadget("rainbow", new RainbowGadget(40, "Rainbow"));
		gadgetManager.addGadget("hellfire", new HellfireGadget(20, "Hellfire"));
		gadgetManager.addGadget("lavatrail", new LavatrailGadget(40, "Lavatrail"));
		gadgetManager.addGadget("rocket", new RocketGadget(15, "Rocket"));
		gadgetManager.addGadget("fireworks", new FireworksGadget(60, "Fireworks"));
	}
	
	private boolean setupPermissions() {
		RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();
		return perms != null;
	}
	
	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		eco = rsp.getProvider();
		return eco != null;
	}
	
	private void loadFiles() {
		Config.load();
		invFile.load();
		try {
			Messages.load();
		} catch (IOException e) {
			System.out.println("Error while loading Messages");
		}
	}
	
	private void loadMySQL() {
		mysql.openConnection();
		
		mysql.createTable("BannedPlayers", "\n`UUID` varchar(36) NOT NULL,"
										 + "\n`Name` varchar(36) NOT NULL,"
										 + "\n`Anfang` bigint(11) NOT NULL,"
										 + "\n`Ende` bigint(11) NOT NULL,"
										 + "\n`Grund` varchar(100) NOT NULL,"
										 + "\n`Author` varchar(36) NOT NULL,"
										 + "\nPRIMARY KEY (`UUID`)\n");
	}
	
	private void loadLogFile() {
		File dataFolder = WickedCore.getInstance().getDataFolder();
		if(!dataFolder.exists()) {
			dataFolder.mkdir();
		}
	
		logFile = new File(WickedCore.getInstance().getDataFolder(), "debugLog.txt");
		if(!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch(IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static WickedCore getInstance() { return INSTANCE; }
	public static InventoryFile getInvFile() { return invFile; }
	public static ServerStatus getServerStatus() { return serverStatus; }
	public static MySQL getMySQL() { return mysql; }
	public static BanManager getBanManager() { return banManager; }
	public static MenuManager getMenuManager() { return menuManager; }
	public static GadgetManager getGadgetManager() { return gadgetManager; }
	public static EffectManager getEffectManager() { return effectManager; }
	public static Permission getPermission() { return perms; }
	public static Economy getEconomy() { return eco; }
	public static GameScheduler getGameScheduler() { return gameScheduler; }
	
	public synchronized void loadUsers() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			WickedUser user = new WickedUser(p);
			new ProfileLoader(user).runTaskAsynchronously(this);
			addUser(user);
		}
	}
	public synchronized void saveUsers() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			WickedUser user = getUser(p);
			new ProfileSaver(user).runTaskAsynchronously(this);
		}
	}
	public static WickedUser getUser(Player player) { return users.get(player.getUniqueId()); }
	public static WickedUser getRemovedUser(Player player) { return users.remove(player.getUniqueId()); }
	public static Collection<WickedUser> getUsers() { return users.values(); }
	public static void addUser(WickedUser user) { users.put(user.getUuid(), user); }
	
	public static BukkitRunnable getTask(Player p) { return onTimeTasks.get(p); }
	public static void addTask(Player p, BukkitRunnable task) { onTimeTasks.put(p, task); }
	public static void removeTask(Player p) { onTimeTasks.remove(p); }
	
	public static List<Plugin> getInstalledWickedPlugins() {
		List<Plugin> wickedPlugins = new ArrayList<>();
		for(Plugin p : Bukkit.getPluginManager().getPlugins()) {
			if(p.getName().contains("Wicked")) {
				wickedPlugins.add(p);
			}
		}
		return wickedPlugins;
	}
	
	public static void debug(Object msg, boolean log) {
		if(Config.DEBUG.getBoolean()) {
			Bukkit.getLogger().info(Messages.DEBUG.getString(msg.toString()));
			if(log) {
				log("[DEBUG] "+msg.toString());
			}
			for(WickedUser u : getUsers()) {
				if(u.isShowDebug()) {
					u.getPlayer().sendMessage(Utils.color("&7&o[DEBUG: "+msg+"]"));
				}
			}
		}
	}
	
	public static void log(String msg) {
		calendar = new GregorianCalendar();
		String timeStamp = "<"+calendar.get(Calendar.DAY_OF_MONTH)
							+". "+calendar.get(Calendar.MONTH)
							+". "+calendar.get(Calendar.HOUR_OF_DAY)
							+":"+calendar.get(Calendar.MINUTE)
							+":"+calendar.get(Calendar.SECOND)+">";
		FileWriter fw;
		PrintWriter pw;
		try {
			fw = new FileWriter(logFile, true);
			pw = new PrintWriter(fw);
			
			pw.println(timeStamp+" "+msg);
			pw.flush();
			pw.close();
		} catch (IOException e) { e.printStackTrace(); }
		
	}
}