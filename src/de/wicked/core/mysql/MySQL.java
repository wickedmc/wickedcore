package de.wicked.core.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.wicked.core.WickedCore;
import de.wicked.core.utils.Config;

public class MySQL {
	
	private Connection connection;
	
	public synchronized boolean openConnection() {
		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://"
							+ Config.DB_HOST.getString()
							+ ":"
							+ Config.DB_PORT.getString()
							+ "/"
							+ Config.DB_DATABASE.getString(),
					Config.DB_USER.getString(),
					Config.DB_PASS.getString());
			WickedCore.debug(
					"Plugin connected to database successfully.", true);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			WickedCore.debug(
					"Error occurred while trying to connect to database.", true);
			return false;
		}
	}
	
	public synchronized void createTable(String name, String args) {
		try {
			this.connection
				.createStatement()
				.execute(
						"CREATE TABLE IF NOT EXISTS `" + name + "` ("
						+ args
						+ ")"
						);
			WickedCore.debug("Successfully created a table: " + name, false);
		} catch (Exception ex) {
			ex.printStackTrace();
			WickedCore.debug("Error while creating a table: " + name, false);
		}
	}
	
	
	public synchronized void closeConnection() {
		try {
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public synchronized Connection getCurrentConnection() {
		try {
			if (this.connection == null || this.connection.isClosed()) {
				openConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return this.connection;
	}
	
	public synchronized boolean isConnected () {
		try {
			return this.connection == null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}
	
	public synchronized void update(String qry) {
			try {
				System.out.println(qry);
				getCurrentConnection().createStatement().executeUpdate(qry);
			} catch (SQLException e) {
				e.printStackTrace();
				WickedCore.debug("Error while executing MySQL Update ("+qry+")", true);
			}
		closeConnection();
	}
	
	public synchronized ResultSet getResult(String qry) {
			try {
				return getCurrentConnection().createStatement().executeQuery(qry);
			} catch (SQLException e) {
				e.printStackTrace();
				WickedCore.debug("Error while executing MySQL Query  ("+qry+")", true);
			}
		closeConnection();
		return null;
	}
	
}
